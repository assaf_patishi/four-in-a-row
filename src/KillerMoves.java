


public class KillerMoves {
	
	
	public int[][] table;
	
	
	
	public KillerMoves(){
		table = new int[35][2];
		
		
	}
	
	
	
	public void initKillers(){
		for(int i = 0; i<table.length; i++){
			for(int j = 0; j<table[i].length; j++){
				table[i][j] = -1;
			}
		}
	}

	
	

	
	public void insertKiller(int depth,int move){
		if(table[depth][0] == -1){
			table[depth][0] = move;
		}
		else{
			if(move != table[depth][0]){
				table[depth][1] = table[depth][0];
				table[depth][0] = move;
			}
		}
	}
	
	
	
	
	public int getKiller(int depth,int position){
		return table[depth][position];
	}

	
	
	
	
}
