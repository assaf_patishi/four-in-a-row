

/*
 * this class holds a hardCoded database for the first 8 ply opening moves (hard level)
 */

public class Book {
	
	
	private static boolean contains(int[]arr,int...indexes){
		if(indexes == null){
			return true;
		}
		int j;
		for(int i = 0; i<indexes.length; i++){
			for(j = 0; j<arr.length; j++){
				if(arr[j]==indexes[i]){
					break;
				}
			}
			if(j==arr.length){
				return false;
			}
		}
		if(arr.length == indexes.length){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	
	
	public static int getMove(int[]p1,int[]p2){
		//1.0  
		if(contains(p1,21) && contains(p2,0)){
			return 22;
		}
		//1.1
		else if(contains(p1,21) && contains(p2,7)){
			return 8;
		}
		//1.2
		else if(contains(p1,21) && contains(p2,14)){
			return 35;
		}
		//1.3
		else if(contains(p1,21) && contains(p2,22)){
			return 23;
		}
		//1.4
		else if(contains(p1,21) && contains(p2,28)){
			return 7;
		}
		//1.5
		else if(contains(p1,21) && contains(p2,35)){
			return 36;
		}
		//1.6
		else if(contains(p1,21) && contains(p2,42)){
			return 22;
		}
		///////////////////////////////////////////////////////////
		//1.0.0
		else if(contains(p1,21,22) && contains(p2,0,1)){
			return 23;
		}
		//1.0.1
		else if(contains(p1,21,22) && contains(p2,0,7)){
			return 23;
		}
		//1.0.2
		else if(contains(p1,21,22) && contains(p2,0,14)){
			return 23;
		}
		//1.0.3
		else if(contains(p1,21,22) && contains(p2,0,23)){
			return 24;
		}
		//1.0.4
		else if(contains(p1,21,22) && contains(p2,0,28)){
			return 23;
		}
		//1.0.5
		else if(contains(p1,21,22) && contains(p2,0,35)){
			return 23;
		}
		//1.0.6
		else if(contains(p1,21,22) && contains(p2,0,42)){
			return 23;
		}
		///////////////////////////////////////////////////////
		//1.1.0
		else if(contains(p1,21,8) && contains(p2,7,0)){
			return 9;
		}
		//1.1.1
		else if(contains(p1,21,8) && contains(p2,7,9)){
			return 22;
		}
		//1.1.2
		else if(contains(p1,21,8) && contains(p2,7,14)){
			return 9;
		}
		//1.1.3
		else if(contains(p1,21,8) && contains(p2,7,22)){
			return 23;
		}
		//1.1.4
		else if(contains(p1,21,8) && contains(p2,7,28)){
			return 22;
		}
		//1.1.5
		else if(contains(p1,21,8) && contains(p2,7,35)){
			return 22;
		}
		//1.1.6
		else if(contains(p1,21,8) && contains(p2,7,42)){
			return 22;
		}
		///////////////////////////////////////////////////////
		//1.2.0
		else if(contains(p1,21,35) && contains(p2,14,0)){
			return 22;
		}
		//1.2.1
		else if(contains(p1,21,35) && contains(p2,14,7)){
			return 22;
		}
		//1.2.2
		else if(contains(p1,21,35) && contains(p2,14,15)){
			return 16;
		}
		//1.2.3
		else if(contains(p1,21,35) && contains(p2,14,22)){
			return 23;
		}
		//1.2.4
		else if(contains(p1,21,35) && contains(p2,14,28)){
			return 22;
		}
		//1.2.5
		else if(contains(p1,21,35) && contains(p2,14,36)){
			return 42;
		}
		//1.2.6
		else if(contains(p1,21,35) && contains(p2,14,42)){
			return 36;
		}
		////////////////////////////////////////////////////////
		//1.3.0
		else if(contains(p1,21,23) && contains(p2,22,0)){
			return 24;
		}
		//1.3.1
		else if(contains(p1,21,23) && contains(p2,22,7)){
			return 24;
		}
		//1.3.2
		else if(contains(p1,21,23) && contains(p2,22,14)){
			return 24;
		}
		//1.3.3
		else if(contains(p1,21,23) && contains(p2,22,24)){
			return 25;
		}
		//1.3.4
		else if(contains(p1,21,23) && contains(p2,22,28)){
			return 24;
		}
		//1.3.5
		else if(contains(p1,21,23) && contains(p2,22,35)){
			return 24;
		}
		//1.3.6
		else if(contains(p1,21,23) && contains(p2,22,42)){
			return 24;
		}
		/////////////////////////////////////////////////////////
		//1.4.0
		else if(contains(p1,21,7) && contains(p2,28,0)){
			return 8;
		}
		//1.4.1
		else if(contains(p1,21,7) && contains(p2,28,8)){
			return 0;
		}
		//1.4.2
		else if(contains(p1,21,7) && contains(p2,28,14)){
			return 22;
		}
		//1.4.3
		else if(contains(p1,21,7) && contains(p2,28,22)){
			return 23;
		}
		//1.4.4
		else if(contains(p1,21,7) && contains(p2,28,29)){
			return 30;
		}
		//1.4.5
		else if(contains(p1,21,7) && contains(p2,28,35)){
			return 22;
		}
		//1.4.6
		else if(contains(p1,21,7) && contains(p2,28,42)){
			return 22;
		}
		////////////////////////////////////////////////////////////////
		//1.5.0
		else if(contains(p1,21,36) && contains(p2,35,0)){
			return 22;
		}
		//1.5.1
		else if(contains(p1,21,36) && contains(p2,35,7)){
			return 22;
		}
		//1.5.2
		else if(contains(p1,21,36) && contains(p2,35,14)){
			return 22;
		}
		//1.5.3
		else if(contains(p1,21,36) && contains(p2,35,22)){
			return 23;
		}
		//1.5.4
		else if(contains(p1,21,36) && contains(p2,35,28)){
			return 37;
		}
		//1.5.5
		else if(contains(p1,21,36) && contains(p2,35,37)){
			return 22;
		}
		//1.5.6
		else if(contains(p1,21,36) && contains(p2,35,42)){
			return 37;
		}
		/////////////////////////////////////////////////////////
		//1.6.0
		else if(contains(p1,21,22) && contains(p2,42,0)){
			return 23;
		}
		//1.6.1
		else if(contains(p1,21,22) && contains(p2,42,7)){
			return 23;
		}
		//1.6.2
		else if(contains(p1,21,22) && contains(p2,42,14)){
			return 23;
		}
		//1.6.3
		else if(contains(p1,21,22) && contains(p2,42,23)){
			return 24;
		}
		//1.6.4
		else if(contains(p1,21,22) && contains(p2,42,28)){
			return 23;
		}
		//1.6.5
		else if(contains(p1,21,22) && contains(p2,42,35)){
			return 23;
		}
		//1.6.6
		else if(contains(p1,21,22) && contains(p2,42,43)){
			return 23;
		}
		//////////////////////////////////////////////////////////
		//1.0.0.0
		else if(contains(p1,21,22,23) && contains(p2,0,1,2)){
			return 24;
		}
		//1.0.0.1
		else if(contains(p1,21,22,23) && contains(p2,0,1,7)){
			return 24;
		}
		//1.0.0.2
		else if(contains(p1,21,22,23) && contains(p2,0,1,14)){
			return 24;
		}
		//1.0.0.3
		else if(contains(p1,21,22,23) && contains(p2,0,1,24)){
			return 25;
		}
		//1.0.0.4
		else if(contains(p1,21,22,23) && contains(p2,0,1,28)){
			return 24;
		}
		//1.0.0.5
		else if(contains(p1,21,22,23) && contains(p2,0,1,35)){
			return 24;
		}
		//1.0.0.6
		else if(contains(p1,21,22,23) && contains(p2,0,1,42)){
			return 24;
		}
		///////////////////////////////////////////////////////////
		//1.0.1.0
		else if(contains(p1,21,22,23) && contains(p2,0,7,1)){
			return 24;
		}
		//1.0.1.1
		else if(contains(p1,21,22,23) && contains(p2,0,7,8)){
			return 24;
		}
		//1.0.1.2
		else if(contains(p1,21,22,23) && contains(p2,0,7,14)){
			return 24;
		}
		//1.0.1.3
		else if(contains(p1,21,22,23) && contains(p2,0,7,24)){
			return 25;
		}
		//1.0.1.4
		else if(contains(p1,21,22,23) && contains(p2,0,7,28)){
			return 24;
		}
		//1.0.1.5
		else if(contains(p1,21,22,23) && contains(p2,0,7,35)){
			return 24;
		}
		//1.0.1.6
		else if(contains(p1,21,22,23) && contains(p2,0,7,42)){
			return 24;
		}
		//////////////////////////////////////////////////////////
		//1.0.2.0
		else if(contains(p1,21,22,23) && contains(p2,0,14,1)){
			return 24;
		}
		//1.0.2.1
		else if(contains(p1,21,22,23) && contains(p2,0,14,7)){
			return 24;
		}
		//1.0.2.2
		else if(contains(p1,21,22,23) && contains(p2,0,14,15)){
			return 24;
		}
		//1.0.2.3
		else if(contains(p1,21,22,23) && contains(p2,0,14,24)){
			return 25;
		}
		//1.0.2.4
		else if(contains(p1,21,22,23) && contains(p2,0,14,28)){
			return 24;
		}
		//1.0.2.5
		else if(contains(p1,21,22,23) && contains(p2,0,14,35)){
			return 24;
		}
		//1.0.2.6
		else if(contains(p1,21,22,23) && contains(p2,0,14,42)){
			return 24;
		}
		///////////////////////////////////////////////////////////////////
		//1.0.3.0
		else if(contains(p1,21,22,24) && contains(p2,0,23,1)){
			return 25;
		}
		//1.0.3.1
		else if(contains(p1,21,22,24) && contains(p2,0,23,7)){
			return 25;
		}
		//1.0.3.2
		else if(contains(p1,21,22,24) && contains(p2,0,23,14)){
			return 25;
		}
		//1.0.3.3
		else if(contains(p1,21,22,24) && contains(p2,0,23,25)){
			return 28;
		}
		//1.0.3.4
		else if(contains(p1,21,22,24) && contains(p2,0,23,28)){
			return 25;
		}
		//1.0.3.5
		else if(contains(p1,21,22,24) && contains(p2,0,23,35)){
			return 25;
		}
		//1.0.3.6
		else if(contains(p1,21,22,24) && contains(p2,0,23,42)){
			return 25;
		}
		//////////////////////////////////////////////////////////////////
		//1.0.4.0
		else if(contains(p1,21,22,23) && contains(p2,0,28,1)){
			return 24;
		}
		//1.0.4.1
		else if(contains(p1,21,22,23) && contains(p2,0,28,7)){
			return 24;
		}
		//1.0.4.2
		else if(contains(p1,21,22,23) && contains(p2,0,28,14)){
			return 24;
		}
		//1.0.4.3
		else if(contains(p1,21,22,23) && contains(p2,0,28,24)){
			return 25;
		}
		//1.0.4.4
		else if(contains(p1,21,22,23) && contains(p2,0,28,29)){
			return 24;
		}
		//1.0.4.5
		else if(contains(p1,21,22,23) && contains(p2,0,28,35)){
			return 24;
		}
		//1.0.4.6
		else if(contains(p1,21,22,23) && contains(p2,0,28,42)){
			return 24;
		}
		///////////////////////////////////////////////////////////////////
		//1.0.5.0
		else if(contains(p1,21,22,23) && contains(p2,0,35,1)){
			return 24;
		}
		//1.0.5.1
		else if(contains(p1,21,22,23) && contains(p2,0,35,7)){
			return 24;
		}
		//1.0.5.2
		else if(contains(p1,21,22,23) && contains(p2,0,35,14)){
			return 24;
		}
		//1.0.5.3
		else if(contains(p1,21,22,23) && contains(p2,0,35,24)){
			return 25;
		}
		//1.0.5.4
		else if(contains(p1,21,22,23) && contains(p2,0,35,28)){
			return 24;
		}
		//1.0.5.5
		else if(contains(p1,21,22,23) && contains(p2,0,35,36)){
			return 24;
		}
		//1.0.5.6
		else if(contains(p1,21,22,23) && contains(p2,0,35,42)){
			return 24;
		}
		//////////////////////////////////////////////////////////////
		//1.0.6.0
		else if(contains(p1,21,22,23) && contains(p2,0,42,1)){
			return 24;
		}
		//1.0.6.1
		else if(contains(p1,21,22,23) && contains(p2,0,42,7)){
			return 24;
		}
		//1.0.6.2
		else if(contains(p1,21,22,23) && contains(p2,0,42,14)){
			return 24;
		}
		//1.0.6.3
		else if(contains(p1,21,22,23) && contains(p2,0,42,24)){
			return 25;
		}
		//1.0.6.4
		else if(contains(p1,21,22,23) && contains(p2,0,42,28)){
			return 24;
		}
		//1.0.6.5
		else if(contains(p1,21,22,23) && contains(p2,0,42,35)){
			return 24;
		}
		//1.0.6.6
		else if(contains(p1,21,22,23) && contains(p2,0,42,43)){
			return 24;
		}
		/////////////////////////////////////////////////////////////////
		//1.1.0.0
		else if(contains(p1,21,8,9) && contains(p2,7,0,1)){
			return 22;
		}
		//1.1.0.1
		else if(contains(p1,21,8,9) && contains(p2,7,0,10)){
			return 11;
		}
		//1.1.0.2
		else if(contains(p1,21,8,9) && contains(p2,7,0,14)){
			return 22;
		}
		//1.1.0.3
		else if(contains(p1,21,8,9) && contains(p2,7,0,22)){
			return 23;
		}
		//1.1.0.4
		else if(contains(p1,21,8,9) && contains(p2,7,0,28)){
			return 22;
		}
		//1.1.0.5
		else if(contains(p1,21,8,9) && contains(p2,7,0,35)){
			return 22;
		}
		//1.1.0.6
		else if(contains(p1,21,8,9) && contains(p2,7,0,42)){
			return 22;
		}
		////////////////////////////////////////////////////////
		//1.1.1.0
		else if(contains(p1,21,8,22) && contains(p2,7,9,0)){
			return 23;
		}
		//1.1.1.1
		else if(contains(p1,21,8,22) && contains(p2,7,9,10)){
			return 23;
		}
		//1.1.1.2
		else if(contains(p1,21,8,22) && contains(p2,7,9,14)){
			return 23;
		}
		//1.1.1.3
		else if(contains(p1,21,8,22) && contains(p2,7,9,23)){
			return 24;
		}
		//1.1.1.4
		else if(contains(p1,21,8,22) && contains(p2,7,9,28)){
			return 23;
		}
		//1.1.1.5
		else if(contains(p1,21,8,22) && contains(p2,7,9,35)){
			return 23;
		}
		//1.1.1.6
		else if(contains(p1,21,8,22) && contains(p2,7,9,42)){
			return 23;
		}
		//////////////////////////////////////////////////////////
		//1.1.2.0
		else if(contains(p1,21,8,9) && contains(p2,7,14,0)){
			return 22;
		}
		//1.1.2.1
		else if(contains(p1,21,8,9) && contains(p2,7,14,10)){
			return 15;
		}
		//1.1.2.2
		else if(contains(p1,21,8,9) && contains(p2,7,14,15)){
			return 16;
		}
		//1.1.2.3
		else if(contains(p1,21,8,9) && contains(p2,7,14,22)){
			return 23;
		}
		//1.1.2.4
		else if(contains(p1,21,8,9) && contains(p2,7,14,28)){
			return 22;
		}
		//1.1.2.5
		else if(contains(p1,21,8,9) && contains(p2,7,14,35)){
			return 22;
		}
		//1.1.2.6
		else if(contains(p1,21,8,9) && contains(p2,7,14,42)){
			return 22;
		}
		////////////////////////////////////////////////////////
		//1.1.3.0
		else if(contains(p1,21,8,23) && contains(p2,7,22,0)){
			return 24;
		}
		//1.1.3.1
		else if(contains(p1,21,8,23) && contains(p2,7,22,9)){
			return 24;
		}
		//1.1.3.2
		else if(contains(p1,21,8,23) && contains(p2,7,22,14)){
			return 24;
		}
		//1.1.3.3
		else if(contains(p1,21,8,23) && contains(p2,7,22,24)){
			return 25;
		}
		//1.1.3.4
		else if(contains(p1,21,8,23) && contains(p2,7,22,28)){
			return 24;
		}
		//1.1.3.5
		else if(contains(p1,21,8,23) && contains(p2,7,22,35)){
			return 24;
		}
		//1.1.3.6
		else if(contains(p1,21,8,23) && contains(p2,7,22,42)){
			return 24;
		}
		/////////////////////////////////////////////////////////////
		//1.1.4.0
		else if(contains(p1,21,8,22) && contains(p2,7,28,0)){
			return 23;
		}
		//1.1.4.1
		else if(contains(p1,21,8,22) && contains(p2,7,28,9)){
			return 23;
		}
		//1.1.4.2
		else if(contains(p1,21,8,22) && contains(p2,7,28,14)){
			return 23;
		}
		//1.1.4.3
		else if(contains(p1,21,8,22) && contains(p2,7,28,23)){
			return 24;
		}
		//1.1.4.4
		else if(contains(p1,21,8,22) && contains(p2,7,28,29)){
			return 23;
		}
		//1.1.4.5
		else if(contains(p1,21,8,22) && contains(p2,7,28,35)){
			return 23;
		}
		//1.1.4.6
		else if(contains(p1,21,8,22) && contains(p2,7,28,42)){
			return 23;
		}
		////////////////////////////////////////////////////////////////
		//1.1.5.0
		else if(contains(p1,21,8,22) && contains(p2,7,35,0)){
			return 23;
		}
		//1.1.5.1
		else if(contains(p1,21,8,22) && contains(p2,7,35,9)){
			return 23;
		}
		//1.1.5.2
		else if(contains(p1,21,8,22) && contains(p2,7,35,14)){
			return 23;
		}
		//1.1.5.3
		else if(contains(p1,21,8,22) && contains(p2,7,35,23)){
			return 24;
		}
		//1.1.5.4
		else if(contains(p1,21,8,22) && contains(p2,7,35,28)){
			return 23;
		}
		//1.1.5.5
		else if(contains(p1,21,8,22) && contains(p2,7,35,36)){
			return 23;
		}
		//1.1.5.6
		else if(contains(p1,21,8,22) && contains(p2,7,35,42)){
			return 23;
		}
		///////////////////////////////////////////////////////////
		//1.1.6.0
		else if(contains(p1,21,8,22) && contains(p2,7,42,0)){
			return 23;
		}
		//1.1.6.1
		else if(contains(p1,21,8,22) && contains(p2,7,42,9)){
			return 23;
		}
		//1.1.6.2
		else if(contains(p1,21,8,22) && contains(p2,7,42,14)){
			return 23;
		}
		//1.1.6.3
		else if(contains(p1,21,8,22) && contains(p2,7,42,23)){
			return 24;
		}
		//1.1.6.4
		else if(contains(p1,21,8,22) && contains(p2,7,42,28)){
			return 23;
		}
		//1.1.6.5
		else if(contains(p1,21,8,22) && contains(p2,7,42,35)){
			return 23;
		}
		//1.1.6.6
		else if(contains(p1,21,8,22) && contains(p2,7,42,43)){
			return 23;
		}
		//////////////////////////////////////////////////////////////
		//1.2.0.0
		else if(contains(p1,21,35,22) && contains(p2,14,0,1)){
			return 23;
		}
		//1.2.0.1
		else if(contains(p1,21,35,22) && contains(p2,14,0,7)){
			return 23;
		}
		//1.2.0.2
		else if(contains(p1,21,35,22) && contains(p2,14,0,15)){
			return 23;
		}
		//1.2.0.3
		else if(contains(p1,21,35,22) && contains(p2,14,0,23)){
			return 24;
		}
		//1.2.0.4
		else if(contains(p1,21,35,22) && contains(p2,14,0,28)){
			return 23;
		}
		//1.2.0.5
		else if(contains(p1,21,35,22) && contains(p2,14,0,36)){
			return 23;
		}
		//1.2.0.6
		else if(contains(p1,21,35,22) && contains(p2,14,0,42)){
			return 23;
		}
		/////////////////////////////////////////////////////////////
		//1.2.1.0
		else if(contains(p1,21,35,22) && contains(p2,14,7,0)){
			return 23;
		}
		//1.2.1.1
		else if(contains(p1,21,35,22) && contains(p2,14,7,8)){
			return 23;
		}
		//1.2.1.2
		else if(contains(p1,21,35,22) && contains(p2,14,7,15)){
			return 23;
		}
		//1.2.1.3
		else if(contains(p1,21,35,22) && contains(p2,14,7,23)){
			return 24;
		}
		//1.2.1.4
		else if(contains(p1,21,35,22) && contains(p2,14,7,28)){
			return 23;
		}
		//1.2.1.5
		else if(contains(p1,21,35,22) && contains(p2,14,7,36)){
			return 23;
		}
		//1.2.1.6
		else if(contains(p1,21,35,22) && contains(p2,14,7,42)){
			return 23;
		}
		///////////////////////////////////////////////////////////////
		//1.2.2.0
		else if(contains(p1,21,35,16) && contains(p2,14,15,0)){
			return 17;
		}
		//1.2.2.1
		else if(contains(p1,21,35,16) && contains(p2,14,15,7)){
			return 17;
		}
		//1.2.2.2
		else if(contains(p1,21,35,16) && contains(p2,14,15,17)){
			return 22;
		}
		//1.2.2.3
		else if(contains(p1,21,35,16) && contains(p2,14,15,22)){
			return 23;
		}
		//1.2.2.4
		else if(contains(p1,21,35,16) && contains(p2,14,15,28)){
			return 22;
		}
		//1.2.2.5
		else if(contains(p1,21,35,16) && contains(p2,14,15,36)){
			return 28;
		}
		//1.2.2.6
		else if(contains(p1,21,35,16) && contains(p2,14,15,42)){
			return 36;
		}
		///////////////////////////////////////////////////////////////
		//1.2.3.0
		else if(contains(p1,21,35,23) && contains(p2,14,22,0)){
			return 24;
		}
		//1.2.3.1
		else if(contains(p1,21,35,23) && contains(p2,14,227,7)){
			return 24;
		}
		//1.2.3.2
		else if(contains(p1,21,35,23) && contains(p2,14,22,15)){
			return 24;
		}
		//1.2.3.3
		else if(contains(p1,21,35,23) && contains(p2,14,22,24)){
			return 25;
		}
		//1.2.3.4
		else if(contains(p1,21,35,23) && contains(p2,14,22,28)){
			return 24;
		}
		//1.2.3.5
		else if(contains(p1,21,35,23) && contains(p2,14,22,36)){
			return 24;
		}
		//1.2.3.6
		else if(contains(p1,21,35,23) && contains(p2,14,22,42)){
			return 24;
		}
		////////////////////////////////////////////////////////////
		//1.2.4.0
		else if(contains(p1,21,35,22) && contains(p2,14,28,0)){
			return 23;
		}
		//1.2.4.1
		else if(contains(p1,21,35,22) && contains(p2,14,28,7)){
			return 23;
		}
		//1.2.4.2
		else if(contains(p1,21,35,22) && contains(p2,14,28,15)){
			return 23;
		}
		//1.2.4.3
		else if(contains(p1,21,35,22) && contains(p2,14,28,23)){
			return 24;
		}
		//1.2.4.4
		else if(contains(p1,21,35,22) && contains(p2,14,28,29)){
			return 23;
		}
		//1.2.4.5
		else if(contains(p1,21,35,22) && contains(p2,14,28,36)){
			return 23;
		}
		//1.2.4.6
		else if(contains(p1,21,35,22) && contains(p2,14,28,42)){
			return 23;
		}
		/////////////////////////////////////////////////////////
		//1.2.5.0
		else if(contains(p1,21,35,42) && contains(p2,14,36,0)){
			return 28;
		}
		//1.2.5.1
		else if(contains(p1,21,35,42) && contains(p2,14,36,7)){
			return 28;
		}
		//1.2.5.2
		else if(contains(p1,21,35,42) && contains(p2,14,36,15)){
			return 28;
		}
		//1.2.5.3
		else if(contains(p1,21,35,42) && contains(p2,14,36,22)){
			return 28;
		}
		//1.2.5.4
		else if(contains(p1,21,35,42) && contains(p2,14,36,28)){
			return 15;
		}
		//1.2.5.5
		else if(contains(p1,21,35,42) && contains(p2,14,36,37)){
			return 28;
		}
		//1.2.5.6
		else if(contains(p1,21,35,42) && contains(p2,14,36,43)){
			return 28;
		}
		//////////////////////////////////////////////////////////////
		//1.2.6.0
		else if(contains(p1,21,35,36) && contains(p2,14,42,0)){
			return 22;
		}
		//1.2.6.1
		else if(contains(p1,21,35,36) && contains(p2,14,42,7)){
			return 22;
		}
		//1.2.6.2
		else if(contains(p1,21,35,36) && contains(p2,14,42,15)){
			return 16;
		}
		//1.2.6.3
		else if(contains(p1,21,35,36) && contains(p2,14,42,22)){
			return 23;
		}
		//1.2.6.4
		else if(contains(p1,21,35,36) && contains(p2,14,42,28)){
			return 22;
		}
		//1.2.6.5
		else if(contains(p1,21,35,36) && contains(p2,14,42,37)){
			return 22;
		}
		//1.2.6.6
		else if(contains(p1,21,35,36) && contains(p2,14,42,43)){
			return 15;
		}
		/////////////////////////////////////////////////////////////
		//1.3.0.0
		else if(contains(p1,21,23,24) && contains(p2,22,0,1)){
			return 25;
		}
		//1.3.0.1
		else if(contains(p1,21,23,24) && contains(p2,22,0,7)){
			return 25;
		}
		//1.3.0.2
		else if(contains(p1,21,23,24) && contains(p2,22,0,14)){
			return 25;
		}
		//1.3.0.3
		else if(contains(p1,21,23,24) && contains(p2,22,0,25)){
			return 14;
		}
		//1.3.0.4
		else if(contains(p1,21,23,24) && contains(p2,22,0,28)){
			return 25;
		}
		//1.3.0.5
		else if(contains(p1,21,23,24) && contains(p2,22,0,35)){
			return 25;
		}
		//1.3.0.6
		else if(contains(p1,21,23,24) && contains(p2,22,0,42)){
			return 25;
		}
		///////////////////////////////////////////////////////////
		//1.3.1.0
		else if(contains(p1,21,23,24) && contains(p2,22,7,0)){
			return 25;
		}
		//1.3.1.1
		else if(contains(p1,21,23,24) && contains(p2,22,7,8)){
			return 25;
		}
		//1.3.1.2
		else if(contains(p1,21,23,24) && contains(p2,22,7,14)){
			return 25;
		}
		//1.3.1.3
		else if(contains(p1,21,23,24) && contains(p2,22,7,25)){
			return 26;
		}
		//1.3.1.4
		else if(contains(p1,21,23,24) && contains(p2,22,7,28)){
			return 25;
		}
		//1.3.1.5
		else if(contains(p1,21,23,24) && contains(p2,22,7,35)){
			return 25;
		}
		//1.3.1.6
		else if(contains(p1,21,23,24) && contains(p2,22,7,42)){
			return 25;
		}
		///////////////////////////////////////////////////////////////
		//1.3.2.0
		else if(contains(p1,21,23,24) && contains(p2,22,14,0)){
			return 25;
		}
		//1.3.2.1
		else if(contains(p1,21,23,24) && contains(p2,22,14,7)){
			return 25;
		}
		//1.3.2.2
		else if(contains(p1,21,23,24) && contains(p2,22,14,15)){
			return 25;
		}
		//1.3.2.3
		else if(contains(p1,21,23,24) && contains(p2,22,14,25)){
			return 15;
		}
		//1.3.2.4
		else if(contains(p1,21,23,24) && contains(p2,22,14,28)){
			return 25;
		}
		//1.3.2.5
		else if(contains(p1,21,23,24) && contains(p2,22,14,35)){
			return 25;
		}
		//1.3.2.6
		else if(contains(p1,21,23,24) && contains(p2,22,14,42)){
			return 25;
		}
		/////////////////////////////////////////////////////////////
		//1.3.3.0
		else if(contains(p1,21,23,25) && contains(p2,22,24,0)){
			return 14;
		}
		//1.3.3.1
		else if(contains(p1,21,23,25) && contains(p2,22,24,7)){
			return 8;
		}
		//1.3.3.2
		else if(contains(p1,21,23,25) && contains(p2,22,24,14)){
			return 15;
		}
		//1.3.3.3
		else if(contains(p1,21,23,25) && contains(p2,22,24,26)){
			return 14;
		}
		//1.3.3.4
		else if(contains(p1,21,23,25) && contains(p2,22,24,28)){
			return 29;
		}
		//1.3.3.5
		else if(contains(p1,21,23,25) && contains(p2,22,24,35)){
			return 36;
		}
		//1.3.3.6
		else if(contains(p1,21,23,25) && contains(p2,22,24,42)){
			return 14;
		}
		/////////////////////////////////////////////////////////////
		//1.3.4.0
		else if(contains(p1,21,23,24) && contains(p2,22,28,0)){
			return 25;
		}
		//1.3.4.1
		else if(contains(p1,21,23,24) && contains(p2,22,28,7)){
			return 25;
		}
		//1.3.4.2
		else if(contains(p1,21,23,24) && contains(p2,22,28,14)){
			return 25;
		}
		//1.3.4.3
		else if(contains(p1,21,23,24) && contains(p2,22,28,25)){
			return 29;
		}
		//1.3.4.4
		else if(contains(p1,21,23,24) && contains(p2,22,28,29)){
			return 25;
		}
		//1.3.4.5
		else if(contains(p1,21,23,24) && contains(p2,22,28,35)){
			return 25;
		}
		//1.3.4.6
		else if(contains(p1,21,23,24) && contains(p2,22,28,42)){
			return 25;
		}
		///////////////////////////////////////////////////////////////
		//1.3.5.0
		else if(contains(p1,21,23,24) && contains(p2,22,35,0)){
			return 25;
		}
		//1.3.5.1
		else if(contains(p1,21,23,24) && contains(p2,22,35,7)){
			return 25;
		}
		//1.3.5.2
		else if(contains(p1,21,23,24) && contains(p2,22,35,14)){
			return 25;
		}
		//1.3.5.3
		else if(contains(p1,21,23,24) && contains(p2,22,35,25)){
			return 26;
		}
		//1.3.5.4
		else if(contains(p1,21,23,24) && contains(p2,22,35,28)){
			return 25;
		}
		//1.3.5.5
		else if(contains(p1,21,23,24) && contains(p2,22,35,36)){
			return 25;
		}
		//1.3.5.6
		else if(contains(p1,21,23,24) && contains(p2,22,35,42)){
			return 25;
		}
		///////////////////////////////////////////////////////////////
		//1.3.6.0
		else if(contains(p1,21,23,24) && contains(p2,22,42,0)){
			return 25;
		}
		//1.3.6.1
		else if(contains(p1,21,23,24) && contains(p2,22,42,7)){
			return 25;
		}
		//1.3.6.2
		else if(contains(p1,21,23,24) && contains(p2,22,42,14)){
			return 25;
		}
		//1.3.6.3
		else if(contains(p1,21,23,24) && contains(p2,22,42,25)){
			return 14;
		}
		//1.3.6.4
		else if(contains(p1,21,23,24) && contains(p2,22,42,28)){
			return 25;
		}
		//1.3.6.5
		else if(contains(p1,21,23,24) && contains(p2,22,42,35)){
			return 25;
		}
		//1.3.6.6
		else if(contains(p1,21,23,24) && contains(p2,22,42,43)){
			return 25;
		}
		/////////////////////////////////////////////////////////////
		//1.4.0.0
		else if(contains(p1,21,7,8) && contains(p2,28,0,1)){
			return 9;
		}
		//1.4.0.1
		else if(contains(p1,21,7,8) && contains(p2,28,0,9)){
			return 22;
		}
		//1.4.0.2
		else if(contains(p1,21,7,8) && contains(p2,28,0,14)){
			return 22;
		}
		//1.4.0.3
		else if(contains(p1,21,7,8) && contains(p2,28,0,22)){
			return 23;
		}
		//1.4.0.4
		else if(contains(p1,21,7,8) && contains(p2,28,0,29)){
			return 30;
		}
		//1.4.0.5
		else if(contains(p1,21,7,8) && contains(p2,28,0,35)){
			return 22;
		}
		//1.4.0.6
		else if(contains(p1,21,7,8) && contains(p2,28,0,42)){
			return 22;
		}
		////////////////////////////////////////////////////////////
		//1.4.1.0
		else if(contains(p1,21,7,0) && contains(p2,28,8,1)){
			return 14;
		}
		//1.4.1.1
		else if(contains(p1,21,7,0) && contains(p2,28,8,9)){
			return 14;
		}
		//1.4.1.2
		else if(contains(p1,21,7,0) && contains(p2,28,8,14)){
			return 15;
		}
		//1.4.1.3
		else if(contains(p1,21,7,0) && contains(p2,28,8,22)){
			return 14;
		}
		//1.4.1.4
		else if(contains(p1,21,7,0) && contains(p2,28,8,29)){
			return 14;
		}
		//1.4.1.5
		else if(contains(p1,21,7,0) && contains(p2,28,8,35)){
			return 14;
		}
		//1.4.1.6
		else if(contains(p1,21,7,0) && contains(p2,28,8,42)){
			return 14;
		}
		/////////////////////////////////////////////////////////
		//1.4.2.0
		else if(contains(p1,21,7,22) && contains(p2,28,14,0)){
			return 23;
		}
		//1.4.2.1
		else if(contains(p1,21,7,22) && contains(p2,28,14,8)){
			return 23;
		}
		//1.4.2.2
		else if(contains(p1,21,7,22) && contains(p2,28,14,15)){
			return 23;
		}
		//1.4.2.3
		else if(contains(p1,21,7,22) && contains(p2,28,14,23)){
			return 24;
		}
		//1.4.2.4
		else if(contains(p1,21,7,22) && contains(p2,28,14,29)){
			return 23;
		}
		//1.4.2.5
		else if(contains(p1,21,7,22) && contains(p2,28,14,35)){
			return 23;
		}
		//1.4.2.6
		else if(contains(p1,21,7,22) && contains(p2,28,14,42)){
			return 23;
		}
		//////////////////////////////////////////////////////////////
		//1.4.3.0
		else if(contains(p1,21,7,23) && contains(p2,28,22,0)){
			return 24;
		}
		//1.4.3.1
		else if(contains(p1,21,7,23) && contains(p2,28,22,8)){
			return 24;
		}
		//1.4.3.2
		else if(contains(p1,21,7,23) && contains(p2,28,22,14)){
			return 24;
		}
		//1.4.3.3
		else if(contains(p1,21,7,23) && contains(p2,28,22,24)){
			return 25;
		}
		//1.4.3.4
		else if(contains(p1,21,7,23) && contains(p2,28,22,29)){
			return 24;
		}
		//1.4.3.5
		else if(contains(p1,21,7,23) && contains(p2,28,22,35)){
			return 24;
		}
		//1.4.3.6
		else if(contains(p1,21,7,23) && contains(p2,28,22,42)){
			return 24;
		}
		////////////////////////////////////////////////////////////
		//1.4.4.0
		else if(contains(p1,21,7,30) && contains(p2,28,29,0)){
			return 8;
		}
		//1.4.4.1
		else if(contains(p1,21,7,30) && contains(p2,28,29,8)){
			return 14;
		}
		//1.4.4.2
		else if(contains(p1,21,7,30) && contains(p2,28,29,14)){
			return 22;
		}
		//1.4.4.3
		else if(contains(p1,21,7,30) && contains(p2,28,29,22)){
			return 23;
		}
		//1.4.4.4
		else if(contains(p1,21,7,30) && contains(p2,28,29,31)){
			return 22;
		}
		//1.4.4.5
		else if(contains(p1,21,7,30) && contains(p2,28,29,35)){
			return 31;
		}
		//1.4.4.6
		else if(contains(p1,21,7,30) && contains(p2,28,29,42)){
			return 31;
		}
		/////////////////////////////////////////////////////////////
		//1.4.5.0
		else if(contains(p1,21,7,22) && contains(p2,28,35,0)){
			return 23;
		}
		//1.4.5.1
		else if(contains(p1,21,7,22) && contains(p2,28,35,8)){
			return 23;
		}
		//1.4.5.2
		else if(contains(p1,21,7,22) && contains(p2,28,35,14)){
			return 23;
		}
		//1.4.5.3
		else if(contains(p1,21,7,22) && contains(p2,28,35,23)){
			return 24;
		}
		//1.4.5.4
		else if(contains(p1,21,7,22) && contains(p2,28,35,29)){
			return 23;
		}
		//1.4.5.5
		else if(contains(p1,21,7,22) && contains(p2,28,35,36)){
			return 23;
		}
		//1.4.5.6
		else if(contains(p1,21,7,22) && contains(p2,28,35,42)){
			return 23;
		}
		/////////////////////////////////////////////////////////////////
		//1.4.6.0
		else if(contains(p1,21,7,22) && contains(p2,28,42,0)){
			return 23;
		}
		//1.4.6.1
		else if(contains(p1,21,7,22) && contains(p2,28,42,8)){
			return 23;
		}
		//1.4.6.2
		else if(contains(p1,21,7,22) && contains(p2,28,42,14)){
			return 23;
		}
		//1.4.6.3
		else if(contains(p1,21,7,22) && contains(p2,28,42,23)){
			return 24;
		}
		//1.4.6.4
		else if(contains(p1,21,7,22) && contains(p2,28,42,29)){
			return 23;
		}
		//1.4.6.5
		else if(contains(p1,21,7,22) && contains(p2,28,42,35)){
			return 23;
		}
		//1.4.6.6
		else if(contains(p1,21,7,22) && contains(p2,28,42,43)){
			return 23;
		}
		////////////////////////////////////////////////////////////
		//1.5.0.0
		else if(contains(p1,21,36,22) && contains(p2,35,0,1)){
			return 23;
		}
		//1.5.0.1
		else if(contains(p1,21,36,22) && contains(p2,35,0,7)){
			return 23;
		}
		//1.5.0.2
		else if(contains(p1,21,36,22) && contains(p2,35,0,14)){
			return 23;
		}
		//1.5.0.3
		else if(contains(p1,21,36,22) && contains(p2,35,0,23)){
			return 24;
		}
		//1.5.0.4
		else if(contains(p1,21,36,22) && contains(p2,35,0,28)){
			return 23;
		}
		//1.5.0.5
		else if(contains(p1,21,36,22) && contains(p2,35,0,37)){
			return 23;
		}
		//1.5.0.6
		else if(contains(p1,21,36,22) && contains(p2,35,0,42)){
			return 23;
		}
		///////////////////////////////////////////////////////////
		//1.5.1.0
		else if(contains(p1,21,36,22) && contains(p2,35,7,0)){
			return 23;
		}
		//1.5.1.1
		else if(contains(p1,21,36,22) && contains(p2,35,7,8)){
			return 23;
		}
		//1.5.1.2
		else if(contains(p1,21,36,22) && contains(p2,35,7,14)){
			return 23;
		}
		//1.5.1.3
		else if(contains(p1,21,36,22) && contains(p2,35,7,23)){
			return 24;
		}
		//1.5.1.4
		else if(contains(p1,21,36,22) && contains(p2,35,7,28)){
			return 23;
		}
		//1.5.1.5
		else if(contains(p1,21,36,22) && contains(p2,35,7,37)){
			return 23;
		}
		//1.5.1.6
		else if(contains(p1,21,36,22) && contains(p2,35,7,42)){
			return 23;
		}
		/////////////////////////////////////////////////////////
		//1.5.2.0
		else if(contains(p1,21,36,22) && contains(p2,35,14,0)){
			return 23;
		}
		//1.5.2.1
		else if(contains(p1,21,36,22) && contains(p2,35,14,7)){
			return 23;
		}
		//1.5.2.2
		else if(contains(p1,21,36,22) && contains(p2,35,14,15)){
			return 23;
		}
		//1.5.2.3
		else if(contains(p1,21,36,22) && contains(p2,35,14,23)){
			return 24;
		}
		//1.5.2.4
		else if(contains(p1,21,36,22) && contains(p2,35,14,28)){
			return 23;
		}
		//1.5.2.5
		else if(contains(p1,21,36,22) && contains(p2,35,14,37)){
			return 23;
		}
		//1.5.2.6
		else if(contains(p1,21,36,22) && contains(p2,35,14,42)){
			return 23;
		}
		////////////////////////////////////////////////////////////
		//1.5.3.0
		else if(contains(p1,21,36,23) && contains(p2,35,22,0)){
			return 24;
		}
		//1.5.3.1
		else if(contains(p1,21,36,23) && contains(p2,35,22,7)){
			return 24;
		}
		//1.5.3.2
		else if(contains(p1,21,36,23) && contains(p2,35,22,14)){
			return 24;
		}
		//1.5.3.3
		else if(contains(p1,21,36,23) && contains(p2,35,22,24)){
			return 25;
		}
		//1.5.3.4
		else if(contains(p1,21,36,23) && contains(p2,35,22,28)){
			return 24;
		}
		//1.5.3.5
		else if(contains(p1,21,36,23) && contains(p2,35,22,37)){
			return 24;
		}
		//1.5.3.6
		else if(contains(p1,21,36,23) && contains(p2,35,22,42)){
			return 24;
		}
		//////////////////////////////////////////////////////////////
		//1.5.4.0
		else if(contains(p1,21,36,37) && contains(p2,35,28,0)){
			return 22;
		}
		//1.5.4.1
		else if(contains(p1,21,36,37) && contains(p2,35,28,7)){
			return 22;
		}
		//1.5.4.2
		else if(contains(p1,21,36,37) && contains(p2,35,28,14)){
			return 22;
		}
		//1.5.4.3
		else if(contains(p1,21,36,37) && contains(p2,35,28,22)){
			return 23;
		}
		//1.5.4.4
		else if(contains(p1,21,36,37) && contains(p2,35,28,29)){
			return 30;
		}
		//1.5.4.5
		else if(contains(p1,21,36,37) && contains(p2,35,28,38)){
			return 29;
		}
		//1.5.4.6
		else if(contains(p1,21,36,37) && contains(p2,35,28,42)){
			return 22;
		}
		/////////////////////////////////////////////////////////////
		//1.5.5.0
		else if(contains(p1,21,36,22) && contains(p2,35,37,0)){
			return 23;
		}
		//1.5.5.1
		else if(contains(p1,21,36,22) && contains(p2,35,37,7)){
			return 23;
		}
		//1.5.5.2
		else if(contains(p1,21,36,22) && contains(p2,35,37,14)){
			return 23;
		}
		//1.5.5.3
		else if(contains(p1,21,36,22) && contains(p2,35,37,23)){
			return 24;
		}
		//1.5.5.4
		else if(contains(p1,21,36,22) && contains(p2,35,37,28)){
			return 23;
		}
		//1.5.5.5
		else if(contains(p1,21,36,22) && contains(p2,35,37,38)){
			return 23;
		}
		//1.5.5.6
		else if(contains(p1,21,36,22) && contains(p2,35,37,42)){
			return 23;
		}
		/////////////////////////////////////////////////////////////
		//1.5.6.0
		else if(contains(p1,21,36,37) && contains(p2,35,42,0)){
			return 22;
		}
		//1.5.6.1
		else if(contains(p1,21,36,37) && contains(p2,35,42,7)){
			return 22;
		}
		//1.5.6.2
		else if(contains(p1,21,36,37) && contains(p2,35,42,14)){
			return 22;
		}
		//1.5.6.3
		else if(contains(p1,21,36,37) && contains(p2,35,42,22)){
			return 23;
		}
		//1.5.6.4
		else if(contains(p1,21,36,37) && contains(p2,35,42,28)){
			return 22;
		}
		//1.5.6.5
		else if(contains(p1,21,36,37) && contains(p2,35,42,38)){
			return 39;
		}
		//1.5.6.6
		else if(contains(p1,21,36,37) && contains(p2,35,42,43)){
			return 22;
		}
		///////////////////////////////////////////////////////////////
		//1.6.0.0
		else if(contains(p1,21,22,23) && contains(p2,42,0,1)){
			return 24;
		}
		//1.6.0.1
		else if(contains(p1,21,22,23) && contains(p2,42,0,7)){
			return 24;
		}
		//1.6.0.2
		else if(contains(p1,21,22,23) && contains(p2,42,0,14)){
			return 24;
		}
		//1.6.0.3
		else if(contains(p1,21,22,23) && contains(p2,42,0,24)){
			return 25;
		}
		//1.6.0.4
		else if(contains(p1,21,22,23) && contains(p2,42,0,28)){
			return 24;
		}
		//1.6.0.5
		else if(contains(p1,21,22,23) && contains(p2,42,0,35)){
			return 24;
		}
		//1.6.0.6
		else if(contains(p1,21,22,23) && contains(p2,42,0,43)){
			return 24;
		}
		//////////////////////////////////////////////////////////////
		//1.6.1.0
		else if(contains(p1,21,22,23) && contains(p2,42,7,0)){
			return 24;
		}
		//1.6.1.1
		else if(contains(p1,21,22,23) && contains(p2,42,7,8)){
			return 24;
		}
		//1.6.1.2
		else if(contains(p1,21,22,23) && contains(p2,42,7,14)){
			return 24;
		}
		//1.6.1.3
		else if(contains(p1,21,22,23) && contains(p2,42,7,24)){
			return 25;
		}
		//1.6.1.4
		else if(contains(p1,21,22,23) && contains(p2,42,7,28)){
			return 24;
		}
		//1.6.1.5
		else if(contains(p1,21,22,23) && contains(p2,42,7,35)){
			return 24;
		}
		//1.6.1.6
		else if(contains(p1,21,22,23) && contains(p2,42,7,43)){
			return 24;
		}
		//////////////////////////////////////////////////////////
		//1.6.2.0
		else if(contains(p1,21,22,23) && contains(p2,42,14,0)){
			return 24;
		}
		//1.6.2.1
		else if(contains(p1,21,22,23) && contains(p2,42,14,7)){
			return 24;
		}
		//1.6.2.2
		else if(contains(p1,21,22,23) && contains(p2,42,14,15)){
			return 24;
		}
		//1.6.2.3
		else if(contains(p1,21,22,23) && contains(p2,42,14,24)){
			return 25;
		}
		//1.6.2.4
		else if(contains(p1,21,22,23) && contains(p2,42,14,28)){
			return 24;
		}
		//1.6.2.5
		else if(contains(p1,21,22,23) && contains(p2,42,14,35)){
			return 24;
		}
		//1.6.2.6
		else if(contains(p1,21,22,23) && contains(p2,42,14,43)){
			return 24;
		}
		///////////////////////////////////////////////////////////
		//1.6.3.0
		else if(contains(p1,21,22,24) && contains(p2,42,23,0)){
			return 25;
		}
		//1.6.3.1
		else if(contains(p1,21,22,24) && contains(p2,42,23,7)){
			return 25;
		}
		//1.6.3.2
		else if(contains(p1,21,22,24) && contains(p2,42,23,14)){
			return 25;
		}
		//1.6.3.3
		else if(contains(p1,21,22,24) && contains(p2,42,23,25)){
			return 14;
		}
		//1.6.3.4
		else if(contains(p1,21,22,24) && contains(p2,42,23,28)){
			return 25;
		}
		//1.6.3.5
		else if(contains(p1,21,22,24) && contains(p2,42,23,35)){
			return 25;
		}
		//1.6.3.6
		else if(contains(p1,21,22,24) && contains(p2,42,23,43)){
			return 25;
		}
		/////////////////////////////////////////////////////////////
		//1.6.4.0
		else if(contains(p1,21,22,23) && contains(p2,42,28,0)){
			return 24;
		}
		//1.6.4.1
		else if(contains(p1,21,22,23) && contains(p2,42,28,7)){
			return 24;
		}
		//1.6.4.2
		else if(contains(p1,21,22,23) && contains(p2,42,28,14)){
			return 24;
		}
		//1.6.4.3
		else if(contains(p1,21,22,23) && contains(p2,42,28,24)){
			return 25;
		}
		//1.6.4.4
		else if(contains(p1,21,22,23) && contains(p2,42,28,29)){
			return 24;
		}
		//1.6.4.5
		else if(contains(p1,21,22,23) && contains(p2,42,28,35)){
			return 24;
		}
		//1.6.4.6
		else if(contains(p1,21,22,23) && contains(p2,42,28,43)){
			return 24;
		}
		///////////////////////////////////////////////////////////////
		//1.6.5.0
		else if(contains(p1,21,22,23) && contains(p2,42,35,0)){
			return 24;
		}
		//1.6.5.1
		else if(contains(p1,21,22,23) && contains(p2,42,35,7)){
			return 24;
		}
		//1.6.5.2
		else if(contains(p1,21,22,23) && contains(p2,42,35,14)){
			return 24;
		}
		//1.6.5.3
		else if(contains(p1,21,22,23) && contains(p2,42,35,24)){
			return 25;
		}
		//1.6.5.4
		else if(contains(p1,21,22,23) && contains(p2,42,35,28)){
			return 24;
		}
		//1.6.5.5
		else if(contains(p1,21,22,23) && contains(p2,42,35,36)){
			return 24;
		}
		//1.6.5.6
		else if(contains(p1,21,22,23) && contains(p2,42,35,43)){
			return 24;
		}
		/////////////////////////////////////////////////////////////
		//1.6.6.0
		else if(contains(p1,21,22,23) && contains(p2,42,43,0)){
			return 24;
		}
		//1.6.6.1
		else if(contains(p1,21,22,23) && contains(p2,42,43,7)){
			return 24;
		}
		//1.6.6.2
		else if(contains(p1,21,22,23) && contains(p2,42,43,14)){
			return 24;
		}
		//1.6.6.3
		else if(contains(p1,21,22,23) && contains(p2,42,43,24)){
			return 25;
		}
		//1.6.6.4
		else if(contains(p1,21,22,23) && contains(p2,42,43,28)){
			return 24;
		}
		//1.6.6.5
		else if(contains(p1,21,22,23) && contains(p2,42,43,35)){
			return 24;
		}
		//1.6.6.6
		else if(contains(p1,21,22,23) && contains(p2,42,43,44)){
			return 24;
		}
        ///////////////////////////////////////////////////////  miscellaneous
		else if(contains(p1,21,23,25,8) && contains(p2,22,24,7,9)){
			return 10;
		}
		else{
			return -1;
		}
	}
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	public static int getMove2(int[]p1,int[]p2){
		//1.0
		if(contains(p1,0) && contains(p2,null)){
			return 21;
		}
		//1.1
		else if(contains(p1,7) && contains(p2,null)){
			return 14;
		}
		//1.2
		else if(contains(p1,14) && contains(p2,null)){
			return 21;
		}
		//1.3
		else if(contains(p1,21) && contains(p2,null)){
			return 22;
		}
		//1.4
		else if(contains(p1,28) && contains(p2,null)){
			return 21;
		}
		//1.5
		else if(contains(p1,35) && contains(p2,null)){
			return 28;
		}
		//1.6
		else if(contains(p1,42) && contains(p2,null)){
			return 21;
		}
		////////////////////////////////////////////////////////
		//1.0.0
		else if(contains(p1,0,1) && contains(p2,21)){
			return 2;
		}
		//1.0.1
		else if(contains(p1,0,7) && contains(p2,21)){
			return 22;
		}
		//1.0.2
		else if(contains(p1,0,14) && contains(p2,21)){
			return 15;
		}
		//1.0.3
		else if(contains(p1,0,22) && contains(p2,21)){
			return 23;
		}
		//1.0.4
		else if(contains(p1,0,28) && contains(p2,21)){
			return 22;
		}
		//1.0.5
		else if(contains(p1,0,35) && contains(p2,21)){
			return 36;
		}
		//1.0.6
		else if(contains(p1,0,42) && contains(p2,21)){
			return 22;
		}
		///////////////////////////////////////////////////
		//1.1.0
		else if(contains(p1,7,0) && contains(p2,14)){
			return 15;
		}
		//1.1.1
		else if(contains(p1,7,8) && contains(p2,14)){
			return 9;
		}
		//1.1.2
		else if(contains(p1,7,15) && contains(p2,14)){
			return 16;
		}
		//1.1.3
		else if(contains(p1,7,21) && contains(p2,14)){
			return 22;
		}
		//1.1.4
		else if(contains(p1,7,28) && contains(p2,14)){
			return 29;
		}
		//1.1.5
		else if(contains(p1,7,35) && contains(p2,14)){
			return 42;
		}
		//1.1.6
		else if(contains(p1,7,42) && contains(p2,14)){
			return 35;
		}
		/////////////////////////////////////////////////////
		//1.2.0
		else if(contains(p1,14,0) && contains(p2,21)){
			return 15;
		}
		//1.2.1
		else if(contains(p1,14,7) && contains(p2,21)){
			return 22;
		}
		//1.2.2
		else if(contains(p1,14,15) && contains(p2,21)){
			return 16;
		}
		//1.2.3
		else if(contains(p1,14,22) && contains(p2,21)){
			return 23;
		}
		//1.2.4
		else if(contains(p1,14,28) && contains(p2,21)){
			return 29;
		}
		//1.2.5
		else if(contains(p1,14,35) && contains(p2,21)){
			return 22;
		}
		//1.2.6
		else if(contains(p1,14,42) && contains(p2,21)){
			return 22;
		}
		///////////////////////////////////////////////////
		//1.3.0
		else if(contains(p1,21,0) && contains(p2,22)){
			return 7;
		}
		//1.3.1
		else if(contains(p1,21,7) && contains(p2,22)){
			return 14;
		}
		//1.3.2
		else if(contains(p1,21,14) && contains(p2,22)){
			return 28;
		}
		//1.3.3
		else if(contains(p1,21,23) && contains(p2,22)){
			return 24;
		}
		//1.3.4
		else if(contains(p1,21,28) && contains(p2,22)){
			return 14;
		}
		//1.3.5
		else if(contains(p1,21,35) && contains(p2,22)){
			return 28;
		}
		//1.3.6
		else if(contains(p1,21,42) && contains(p2,22)){
			return 35;
		}
		//////////////////////////////////////////////////////////
		//1.4.0
		else if(contains(p1,28,0) && contains(p2,21)){
			return 22;
		}
		//1.4.1
		else if(contains(p1,28,7) && contains(p2,21)){
			return 22;
		}
		//1.4.2
		else if(contains(p1,28,14) && contains(p2,21)){
			return 29;
		}
		//1.4.3
		else if(contains(p1,28,22) && contains(p2,21)){
			return 23;
		}
		//1.4.4
		else if(contains(p1,28,29) && contains(p2,21)){
			return 30;
		}
		//1.4.5
		else if(contains(p1,28,35) && contains(p2,21)){
			return 22;
		}
		//1.4.6
		else if(contains(p1,28,42) && contains(p2,21)){
			return 29;
		}
		//////////////////////////////////////////////////////
		//1.5.0
		else if(contains(p1,35,0) && contains(p2,28)){
			return 7;
		}
		//1.5.1
		else if(contains(p1,35,7) && contains(p2,28)){
			return 0;
		}
		//1.5.2
		else if(contains(p1,35,14) && contains(p2,28)){
			return 15;
		}
		//1.5.3
		else if(contains(p1,35,21) && contains(p2,28)){
			return 22;
		}
		//1.5.4
		else if(contains(p1,35,29) && contains(p2,28)){
			return 30;
		}
		//1.5.5
		else if(contains(p1,35,36) && contains(p2,28)){
			return 37;
		}
		//1.5.6
		else if(contains(p1,35,42) && contains(p2,28)){
			return 29;
		}
		/////////////////////////////////////////////////////////
		//1.6.0
		else if(contains(p1,42,0) && contains(p2,21)){
			return 1;
		}
		//1.6.1
		else if(contains(p1,42,7) && contains(p2,21)){
			return 22;
		}
		//1.6.2
		else if(contains(p1,42,14) && contains(p2,21)){
			return 22;
		}
		//1.6.3
		else if(contains(p1,42,22) && contains(p2,21)){
			return 23;
		}
		//1.6.4
		else if(contains(p1,42,28) && contains(p2,21)){
			return 29;
		}
		//1.6.5
		else if(contains(p1,42,35) && contains(p2,21)){
			return 22;
		}
		//1.6.6
		else if(contains(p1,42,43) && contains(p2,21)){
			return 44;
		}
		//////////////////////////////////////////////////////
		//1.0.0.0
		else if(contains(p1,0,1,3) && contains(p2,21,2)){
			return 4;
		}
		//1.0.0.1
		else if(contains(p1,0,1,7) && contains(p2,21,2)){
			return 8;
		}
		//1.0.0.2
		else if(contains(p1,0,1,14) && contains(p2,21,2)){
			return 15;
		}
		//1.0.0.3
		else if(contains(p1,0,1,22) && contains(p2,21,2)){
			return 23;
		}
		//1.0.0.4
		else if(contains(p1,0,1,28) && contains(p2,21,2)){
			return 22;
		}
		//1.0.0.5
		else if(contains(p1,0,1,35) && contains(p2,21,2)){
			return 22;
		}
		//1.0.0.6
		else if(contains(p1,0,1,42) && contains(p2,21,2)){
			return 4;
		}
		////////////////////////////////////////////////////////
		//1.0.1.0
		else if(contains(p1,0,7,1) && contains(p2,21,22)){
			return 2;
		}
		//1.0.1.1
		else if(contains(p1,0,7,8) && contains(p2,21,22)){
			return 9;
		}
		//1.0.1.2
		else if(contains(p1,0,7,14) && contains(p2,21,22)){
			return 15;
		}
		//1.0.1.3
		else if(contains(p1,0,7,23) && contains(p2,21,22)){
			return 24;
		}
		//1.0.1.4
		else if(contains(p1,0,7,28) && contains(p2,21,22)){
			return 29;
		}
		//1.0.1.5
		else if(contains(p1,0,7,35) && contains(p2,21,22)){
			return 36;
		}
		//1.0.1.6
		else if(contains(p1,0,7,42) && contains(p2,21,22)){
			return 1;
		}
		/////////////////////////////////////////////////////////
		//1.0.2.0
		else if(contains(p1,0,14,1) && contains(p2,21,15)){
			return 2;
		}
		//1.0.2.1
		else if(contains(p1,0,14,7) && contains(p2,21,15)){
			return 22;
		}
		//1.0.2.2
		else if(contains(p1,0,14,16) && contains(p2,21,15)){
			return 1;
		}
		//1.0.2.3
		else if(contains(p1,0,14,22) && contains(p2,21,15)){
			return 23;
		}
		//1.0.2.4
		else if(contains(p1,0,14,28) && contains(p2,21,15)){
			return 22;
		}
		//1.0.2.5
		else if(contains(p1,0,14,35) && contains(p2,21,15)){
			return 22;
		}
		//1.0.2.6
		else if(contains(p1,0,14,42) && contains(p2,21,15)){
			return 1;
		}
		///////////////////////////////////////////////////////
		//1.0.3.0
		else if(contains(p1,0,22,1) && contains(p2,21,23)){
			return 2;
		}
		//1.0.3.1
		else if(contains(p1,0,22,7) && contains(p2,21,23)){
			return 8;
		}
		//1.0.3.2
		else if(contains(p1,0,22,14) && contains(p2,21,23)){
			return 1;
		}
		//1.0.3.3
		else if(contains(p1,0,22,24) && contains(p2,21,23)){
			return 25;
		}
		//1.0.3.4
		else if(contains(p1,0,22,28) && contains(p2,21,23)){
			return 29;
		}
		//1.0.3.5
		else if(contains(p1,0,22,35) && contains(p2,21,23)){
			return 24;
		}
		//1.0.3.6
		else if(contains(p1,0,22,42) && contains(p2,21,23)){
			return 24;
		}
		////////////////////////////////////////////////////////
		//1.0.4.0
		else if(contains(p1,0,28,1) && contains(p2,21,22)){
			return 29;
		}
		//1.0.4.1
		else if(contains(p1,0,28,7) && contains(p2,21,22)){
			return 1;
		}
		//1.0.4.2
		else if(contains(p1,0,28,14) && contains(p2,21,22)){
			return 15;
		}
		//1.0.4.3
		else if(contains(p1,0,28,23) && contains(p2,21,22)){
			return 29;
		}
		//1.0.4.4
		else if(contains(p1,0,28,29) && contains(p2,21,22)){
			return 23;
		}
		//1.0.4.5
		else if(contains(p1,0,28,35) && contains(p2,21,22)){
			return 29;
		}
		//1.0.4.6
		else if(contains(p1,0,28,42) && contains(p2,21,22)){
			return 29;
		}
		////////////////////////////////////////////////////////////////
		//1.0.5.0
		else if(contains(p1,0,35,1) && contains(p2,21,36)){
			return 2;
		}
		//1.0.5.1
		else if(contains(p1,0,35,7) && contains(p2,21,36)){
			return 22;
		}
		//1.0.5.2
		else if(contains(p1,0,35,14) && contains(p2,21,36)){
			return 15;
		}
		//1.0.5.3
		else if(contains(p1,0,35,22) && contains(p2,21,36)){
			return 23;
		}
		//1.0.5.4
		else if(contains(p1,0,35,28) && contains(p2,21,36)){
			return 29;
		}
		//1.0.5.5
		else if(contains(p1,0,35,37) && contains(p2,21,36)){
			return 38;
		}
		//1.0.5.6
		else if(contains(p1,0,35,42) && contains(p2,21,36)){
			return 43;
		}
		///////////////////////////////////////////////////////////
		//1.0.6.0
		else if(contains(p1,0,42,1) && contains(p2,21,22)){
			return 2;
		}
		//1.0.6.1
		else if(contains(p1,0,42,7) && contains(p2,21,22)){
			return 35;
		}
		//1.0.6.2
		else if(contains(p1,0,42,14) && contains(p2,21,22)){
			return 15;
		}
		//1.0.6.3
		else if(contains(p1,0,42,23) && contains(p2,21,22)){
			return 24;
		}
		//1.0.6.4
		else if(contains(p1,0,42,28) && contains(p2,21,22)){
			return 29;
		}
		//1.0.6.5
		else if(contains(p1,0,42,35) && contains(p2,21,22)){
			return 7;
		}
		//1.0.6.6
		else if(contains(p1,0,42,43) && contains(p2,21,22)){
			return 44;
		}
		///////////////////////////////////////////////////////////
		//1.1.0.0
		else if(contains(p1,7,0,1) && contains(p2,14,15)){
			return 2;
		}
		//1.1.0.1
		else if(contains(p1,7,0,8) && contains(p2,14,15)){
			return 9;
		}
		//1.1.0.2
		else if(contains(p1,7,0,16) && contains(p2,14,15)){
			return 17;
		}
		//1.1.0.3
		else if(contains(p1,7,0,21) && contains(p2,14,15)){
			return 22;
		}
		//1.1.0.4
		else if(contains(p1,7,0,28) && contains(p2,14,15)){
			return 35;
		}
		//1.1.0.5
		else if(contains(p1,7,0,35) && contains(p2,14,15)){
			return 28;
		}
		//1.1.0.6
		else if(contains(p1,7,0,42) && contains(p2,14,15)){
			return 35;
		}
		//////////////////////////////////////////////////////
		//1.1.1.0
		else if(contains(p1,7,8,0) && contains(p2,14,9)){
			return 15;
		}
		//1.1.1.1
		else if(contains(p1,7,8,10) && contains(p2,14,9)){
			return 11;
		}
		//1.1.1.2
		else if(contains(p1,7,8,15) && contains(p2,14,9)){
			return 16;
		}
		//1.1.1.3
		else if(contains(p1,7,8,21) && contains(p2,14,9)){
			return 22;
		}
		//1.1.1.4
		else if(contains(p1,7,8,28) && contains(p2,14,9)){
			return 29;
		}
		//1.1.1.5
		else if(contains(p1,7,8,35) && contains(p2,14,9)){
			return 42;
		}
		//1.1.1.6
		else if(contains(p1,7,8,42) && contains(p2,14,9)){
			return 35;
		}
		////////////////////////////////////////////////////
		//1.1.2.0
		else if(contains(p1,7,15,0) && contains(p2,14,16)){
			return 8;
		}
		//1.1.2.1
		else if(contains(p1,7,15,8) && contains(p2,14,16)){
			return 9;
		}
		//1.1.2.2
		else if(contains(p1,7,15,17) && contains(p2,14,16)){
			return 18;
		}
		//1.1.2.3
		else if(contains(p1,7,15,21) && contains(p2,14,16)){
			return 22;
		}
		//1.1.2.4
		else if(contains(p1,7,15,28) && contains(p2,14,16)){
			return 29;
		}
		//1.1.2.5
		else if(contains(p1,7,15,35) && contains(p2,14,16)){
			return 36;
		}
		//1.1.2.6
		else if(contains(p1,7,15,42) && contains(p2,14,16)){
			return 35;
		}
		//////////////////////////////////////////////////////////
		//1.1.3.0
		else if(contains(p1,7,21,0) && contains(p2,14,22)){
			return 15;
		}
		//1.1.3.1
		else if(contains(p1,7,21,8) && contains(p2,14,22)){
			return 9;
		}
		//1.1.3.2
		else if(contains(p1,7,21,15) && contains(p2,14,22)){
			return 23;
		}
		//1.1.3.3
		else if(contains(p1,7,21,23) && contains(p2,14,22)){
			return 24;
		}
		//1.1.3.4
		else if(contains(p1,7,21,28) && contains(p2,14,22)){
			return 29;
		}
		//1.1.3.5
		else if(contains(p1,7,21,35) && contains(p2,14,22)){
			return 42;
		}
		//1.1.3.6
		else if(contains(p1,7,21,42) && contains(p2,14,22)){
			return 35;
		}
		///////////////////////////////////////////////////////////
		//1.1.4.0
		else if(contains(p1,7,28,0) && contains(p2,14,29)){
			return 15;
		}
		//1.1.4.1
		else if(contains(p1,7,28,8) && contains(p2,14,29)){
			return 9;
		}
		//1.1.4.2
		else if(contains(p1,7,28,15) && contains(p2,14,29)){
			return 16;
		}
		//1.1.4.3
		else if(contains(p1,7,28,21) && contains(p2,14,29)){
			return 22;
		}
		//1.1.4.4
		else if(contains(p1,7,28,30) && contains(p2,14,29)){
			return 31;
		}
		//1.1.4.5
		else if(contains(p1,7,28,35) && contains(p2,14,29)){
			return 42;
		}
		//1.1.4.6
		else if(contains(p1,7,28,42) && contains(p2,14,29)){
			return 35;
		}
		//////////////////////////////////////////////////////
		//1.1.5.0
		else if(contains(p1,7,35,0) && contains(p2,14,42)){
			return 15;
		}
		//1.1.5.1
		else if(contains(p1,7,35,8) && contains(p2,14,42)){
			return 9;
		}
		//1.1.5.2
		else if(contains(p1,7,35,15) && contains(p2,14,42)){
			return 16;
		}
		//1.1.5.3
		else if(contains(p1,7,35,21) && contains(p2,14,42)){
			return 22;
		}
		//1.1.5.4
		else if(contains(p1,7,35,28) && contains(p2,14,42)){
			return 29;
		}
		//1.1.5.5
		else if(contains(p1,7,35,36) && contains(p2,14,42)){
			return 37;
		}
		//1.1.5.6
		else if(contains(p1,7,35,43) && contains(p2,14,42)){
			return 36;
		}
		/////////////////////////////////////////////////////////
		//1.1.6.0
		else if(contains(p1,7,42,0) && contains(p2,14,35)){
			return 15;
		}
		//1.1.6.1
		else if(contains(p1,7,42,8) && contains(p2,14,35)){
			return 9;
		}
		//1.1.6.2
		else if(contains(p1,7,42,15) && contains(p2,14,35)){
			return 16;
		}
		//1.1.6.3
		else if(contains(p1,7,42,21) && contains(p2,14,35)){
			return 22;
		}
		//1.1.6.4
		else if(contains(p1,7,42,28) && contains(p2,14,35)){
			return 29;
		}
		//1.1.6.5
		else if(contains(p1,7,42,36) && contains(p2,14,35)){
			return 37;
		}
		//1.1.6.6
		else if(contains(p1,7,42,43) && contains(p2,14,35)){
			return 15;
		}
		///////////////////////////////////////////////////////////
		//1.2.0.0
		else if(contains(p1,14,0,1) && contains(p2,21,15)){
			return 2;
		}
		//1.2.0.1
		else if(contains(p1,14,0,7) && contains(p2,21,15)){
			return 22;
		}
		//1.2.0.2
		else if(contains(p1,14,0,16) && contains(p2,21,15)){
			return 17;
		}
		//1.2.0.3
		else if(contains(p1,14,0,22) && contains(p2,21,15)){
			return 23;
		}
		//1.2.0.4
		else if(contains(p1,14,0,28) && contains(p2,21,15)){
			return 22;
		}
		//1.2.0.5
		else if(contains(p1,14,0,35) && contains(p2,21,15)){
			return 22;
		}
		//1.2.0.6
		else if(contains(p1,14,0,42) && contains(p2,21,15)){
			return 22;
		}
		//////////////////////////////////////////////////////////
		//1.2.1.0
		else if(contains(p1,14,7,0) && contains(p2,21,22)){
			return 15;
		}
		//1.2.1.1
		else if(contains(p1,14,7,8) && contains(p2,21,22)){
			return 9;
		}
		//1.2.1.2
		else if(contains(p1,14,7,15) && contains(p2,21,22)){
			return 16;
		}
		//1.2.1.3
		else if(contains(p1,14,7,23) && contains(p2,21,22)){
			return 24;
		}
		//1.2.1.4
		else if(contains(p1,14,7,28) && contains(p2,21,22)){
			return 15;
		}
		//1.2.1.5
		else if(contains(p1,14,7,35) && contains(p2,21,22)){
			return 15;
		}
		//1.2.1.6
		else if(contains(p1,14,7,42) && contains(p2,21,22)){
			return 15;
		}
		//////////////////////////////////////////////////////////
		//1.2.2.0
		else if(contains(p1,14,15,0) && contains(p2,21,16)){
			return 17;
		}
		//1.2.2.1
		else if(contains(p1,14,15,7) && contains(p2,21,16)){
			return 8;
		}
		//1.2.2.2
		else if(contains(p1,14,15,17) && contains(p2,21,16)){
			return 18;
		}
		//1.2.2.3
		else if(contains(p1,14,15,22) && contains(p2,21,16)){
			return 23;
		}
		//1.2.2.4
		else if(contains(p1,14,15,28) && contains(p2,21,16)){
			return 29;
		}
		//1.2.2.5
		else if(contains(p1,14,15,35) && contains(p2,21,16)){
			return 36;
		}
		//1.2.2.6
		else if(contains(p1,14,15,42) && contains(p2,21,16)){
			return 22;
		}
		////////////////////////////////////////////////////////////
		//1.2.2.0
		else if(contains(p1,14,15,0) && contains(p2,21,16)){
			return 1;
		}
		//1.2.2.1
		else if(contains(p1,14,15,7) && contains(p2,21,16)){
			return 8;
		}
		//1.2.2.2
		else if(contains(p1,14,15,17) && contains(p2,21,16)){
			return 18;
		}
		//1.2.2.3
		else if(contains(p1,14,15,22) && contains(p2,21,16)){
			return 23;
		}
		//1.2.2.4
		else if(contains(p1,14,15,28) && contains(p2,21,16)){
			return 29;
		}
		//1.2.2.5
		else if(contains(p1,14,15,35) && contains(p2,21,16)){
			return 36;
		}
		//1.2.2.6
		else if(contains(p1,14,15,42) && contains(p2,21,16)){
			return 17;
		}
		/////////////////////////////////////////////////////////////
		//1.2.3.0
		else if(contains(p1,14,22,0) && contains(p2,21,23)){
			return 15;
		}
		//1.2.3.1
		else if(contains(p1,14,22,7) && contains(p2,21,23)){
			return 15;
		}
		//1.2.3.2
		else if(contains(p1,14,22,15) && contains(p2,21,23)){
			return 16;
		}
		//1.2.3.3
		else if(contains(p1,14,22,24) && contains(p2,21,23)){
			return 25;
		}
		//1.2.3.4
		else if(contains(p1,14,22,28) && contains(p2,21,23)){
			return 15;
		}
		//1.2.3.5
		else if(contains(p1,14,22,35) && contains(p2,21,23)){
			return 36;
		}
		//1.2.3.6
		else if(contains(p1,14,22,42) && contains(p2,21,23)){
			return 24;
		}
		//////////////////////////////////////////////////////////////
		//1.2.4.0
		else if(contains(p1,14,28,0) && contains(p2,21,29)){
			return 1;
		}
		//1.2.4.1
		else if(contains(p1,14,28,7) && contains(p2,21,29)){
			return 22;
		}
		//1.2.4.2
		else if(contains(p1,14,28,15) && contains(p2,21,29)){
			return 16;
		}
		//1.2.4.3
		else if(contains(p1,14,28,22) && contains(p2,21,29)){
			return 23;
		}
		//1.2.4.4
		else if(contains(p1,14,28,30) && contains(p2,21,29)){
			return 31;
		}
		//1.2.4.5
		else if(contains(p1,14,28,35) && contains(p2,21,29)){
			return 36;
		}
		//1.2.4.6
		else if(contains(p1,14,28,42) && contains(p2,21,29)){
			return 22;
		}
		//////////////////////////////////////////////////////////
		//1.2.5.0
		else if(contains(p1,14,35,0) && contains(p2,21,22)){
			return 15;
		}
		//1.2.5.1
		else if(contains(p1,14,35,7) && contains(p2,21,22)){
			return 15;
		}
		//1.2.5.2
		else if(contains(p1,14,35,15) && contains(p2,21,22)){
			return 16;
		}
		//1.2.5.3
		else if(contains(p1,14,35,23) && contains(p2,21,22)){
			return 24;
		}
		//1.2.5.4
		else if(contains(p1,14,35,28) && contains(p2,21,22)){
			return 15;
		}
		//1.2.5.5
		else if(contains(p1,14,35,36) && contains(p2,21,22)){
			return 37;
		}
		//1.2.5.6
		else if(contains(p1,14,35,42) && contains(p2,21,22)){
			return 15;
		}
		//////////////////////////////////////////////////////////
		//1.2.6.0
		else if(contains(p1,14,42,0) && contains(p2,21,22)){
			return 15;
		}
		//1.2.6.1
		else if(contains(p1,14,42,7) && contains(p2,21,22)){
			return 15;
		}
		//1.2.6.2
		else if(contains(p1,14,42,15) && contains(p2,21,22)){
			return 23;
		}
		//1.2.6.3
		else if(contains(p1,14,42,23) && contains(p2,21,22)){
			return 24;
		}
		//1.2.6.4
		else if(contains(p1,14,42,28) && contains(p2,21,22)){
			return 29;
		}
		//1.2.6.5
		else if(contains(p1,14,42,35) && contains(p2,21,22)){
			return 15;
		}
		//1.2.6.6
		else if(contains(p1,14,42,43) && contains(p2,21,22)){
			return 44;
		}
		//////////////////////////////////////////////////////////
		//1.3.0.0
		else if(contains(p1,21,0,1) && contains(p2,22,7)){
			return 2;
		}
		//1.3.0.1
		else if(contains(p1,21,0,8) && contains(p2,22,7)){
			return 9;
		}
		//1.3.0.2
		else if(contains(p1,21,0,14) && contains(p2,22,7)){
			return 28;
		}
		//1.3.0.3
		else if(contains(p1,21,0,23) && contains(p2,22,7)){
			return 24;
		}
		//1.3.0.4
		else if(contains(p1,21,0,28) && contains(p2,22,7)){
			return 35;
		}
		//1.3.0.5
		else if(contains(p1,21,0,35) && contains(p2,22,7)){
			return 28;
		}
		//1.3.0.6
		else if(contains(p1,21,0,42) && contains(p2,22,7)){
			return 35;
		}
		///////////////////////////////////////////////////////
		//1.3.1.0
		else if(contains(p1,21,7,0) && contains(p2,22,14)){
			return 15;
		}
		//1.3.1.1
		else if(contains(p1,21,7,8) && contains(p2,22,14)){
			return 9;
		}
		//1.3.1.2
		else if(contains(p1,21,7,15) && contains(p2,22,14)){
			return 23;
		}
		//1.3.1.3
		else if(contains(p1,21,7,23) && contains(p2,22,14)){
			return 24;
		}
		//1.3.1.4
		else if(contains(p1,21,7,28) && contains(p2,22,14)){
			return 29;
		}
		//1.3.1.5
		else if(contains(p1,21,7,35) && contains(p2,22,14)){
			return 42;
		}
		//1.3.1.6
		else if(contains(p1,21,7,42) && contains(p2,22,14)){
			return 35;
		}
		///////////////////////////////////////////////////////
		//1.3.2.0
		else if(contains(p1,21,14,0) && contains(p2,22,28)){
			return 7;
		}
		//1.3.2.1
		else if(contains(p1,21,14,7) && contains(p2,22,28)){
			return 0;
		}
		//1.3.2.2
		else if(contains(p1,21,14,15) && contains(p2,22,28)){
			return 23;
		}
		//1.3.2.3
		else if(contains(p1,21,14,23) && contains(p2,22,28)){
			return 29;
		}
		//1.3.2.4
		else if(contains(p1,21,14,29) && contains(p2,22,28)){
			return 23;
		}
		//1.3.2.5
		else if(contains(p1,21,14,35) && contains(p2,22,28)){
			return 15;
		}
		//1.3.2.6
		else if(contains(p1,21,14,42) && contains(p2,22,28)){
			return 0;
		}
		///////////////////////////////////////////////////////
		//1.3.3.0
		else if(contains(p1,21,23,0) && contains(p2,22,24)){
			return 7;
		}
		//1.3.3.1
		else if(contains(p1,21,23,7) && contains(p2,22,24)){
			return 14;
		}
		//1.3.3.2
		else if(contains(p1,21,23,14) && contains(p2,22,24)){
			return 7;
		}
		//1.3.3.3
		else if(contains(p1,21,23,25) && contains(p2,22,24)){
			return 7;
		}
		//1.3.3.4
		else if(contains(p1,21,23,28) && contains(p2,22,24)){
			return 35;
		}
		//1.3.3.5
		else if(contains(p1,21,23,35) && contains(p2,22,24)){
			return 28;
		}
		//1.3.3.6
		else if(contains(p1,21,23,42) && contains(p2,22,24)){
			return 35;
		}
		/////////////////////////////////////////////////////////
		//1.3.4.0
		else if(contains(p1,21,28,0) && contains(p2,22,14)){
			return 42;
		}
		//1.3.4.1
		else if(contains(p1,21,28,7) && contains(p2,22,14)){
			return 29;
		}
		//1.3.4.2
		else if(contains(p1,21,28,15) && contains(p2,22,14)){
			return 23;
		}
		//1.3.4.3
		else if(contains(p1,21,28,23) && contains(p2,22,14)){
			return 15;
		}
		//1.3.4.4
		else if(contains(p1,21,28,29) && contains(p2,22,14)){
			return 23;
		}
		//1.3.4.5
		else if(contains(p1,21,28,35) && contains(p2,22,14)){
			return 42;
		}
		//1.3.4.6
		else if(contains(p1,21,28,42) && contains(p2,22,14)){
			return 35;
		}
		//////////////////////////////////////////////////////////
		//1.3.5.0
		else if(contains(p1,21,35,0) && contains(p2,22,28)){
			return 7;
		}
		//1.3.5.1
		else if(contains(p1,21,35,7) && contains(p2,22,28)){
			return 0;
		}
		//1.3.5.2
		else if(contains(p1,21,35,14) && contains(p2,22,28)){
			return 15;
		}
		//1.3.5.3
		else if(contains(p1,21,35,23) && contains(p2,22,28)){
			return 24;
		}
		//1.3.5.4
		else if(contains(p1,21,35,29) && contains(p2,22,28)){
			return 23;
		}
		//1.3.5.5
		else if(contains(p1,21,35,36) && contains(p2,22,28)){
			return 37;
		}
		//1.3.5.6
		else if(contains(p1,21,35,42) && contains(p2,22,28)){
			return 29;
		}
		//////////////////////////////////////////////////////////
		//1.3.6.0
		else if(contains(p1,21,42,0) && contains(p2,22,35)){
			return 7;
		}
		//1.3.6.1
		else if(contains(p1,21,42,7) && contains(p2,22,35)){
			return 14;
		}
		//1.3.6.2
		else if(contains(p1,21,42,14) && contains(p2,22,35)){
			return 7;
		}
		//1.3.6.3
		else if(contains(p1,21,42,23) && contains(p2,22,35)){
			return 24;
		}
		//1.3.6.4
		else if(contains(p1,21,42,28) && contains(p2,22,35)){
			return 14;
		}
		//1.3.6.5
		else if(contains(p1,21,42,36) && contains(p2,22,35)){
			return 37;
		}
		//1.3.6.6
		else if(contains(p1,21,42,43) && contains(p2,22,35)){
			return 44;
		}
		//////////////////////////////////////////////////////////
		//1.4.0.0
		else if(contains(p1,28,0,1) && contains(p2,21,22)){
			return 29;
		}
		//1.4.0.1
		else if(contains(p1,28,0,7) && contains(p2,21,22)){
			return 29;
		}
		//1.4.0.2
		else if(contains(p1,28,0,14) && contains(p2,21,22)){
			return 23;
		}
		//1.4.0.3
		else if(contains(p1,28,0,23) && contains(p2,21,22)){
			return 29;
		}
		//1.4.0.4
		else if(contains(p1,28,0,29) && contains(p2,21,22)){
			return 23;
		}
		//1.4.0.5
		else if(contains(p1,28,0,35) && contains(p2,21,22)){
			return 29;
		}
		//1.4.0.6
		else if(contains(p1,28,0,42) && contains(p2,21,22)){
			return 29;
		}
		/////////////////////////////////////////////////////////
		//1.4.1.0
		else if(contains(p1,28,7,0) && contains(p2,21,22)){
			return 29;
		}
		//1.4.1.1
		else if(contains(p1,28,7,8) && contains(p2,21,22)){
			return 9;
		}
		//1.4.1.2
		else if(contains(p1,28,7,14) && contains(p2,21,22)){
			return 15;
		}
		//1.4.1.3
		else if(contains(p1,28,7,23) && contains(p2,21,22)){
			return 24;
		}
		//1.4.1.4
		else if(contains(p1,28,7,29) && contains(p2,21,22)){
			return 30;
		}
		//1.4.1.5
		else if(contains(p1,28,7,35) && contains(p2,21,22)){
			return 29;
		}
		//1.4.1.6
		else if(contains(p1,28,7,42) && contains(p2,21,22)){
			return 29;
		}
		//////////////////////////////////////////////////////
		//1.4.2.0
		else if(contains(p1,28,14,0) && contains(p2,21,29)){
			return 1;
		}
		//1.4.2.1
		else if(contains(p1,28,14,7) && contains(p2,21,29)){
			return 22;
		}
		//1.4.2.2
		else if(contains(p1,28,14,15) && contains(p2,21,29)){
			return 16;
		}
		//1.4.2.3
		else if(contains(p1,28,14,22) && contains(p2,21,29)){
			return 23;
		}
		//1.4.2.4
		else if(contains(p1,28,14,30) && contains(p2,21,29)){
			return 31;
		}
		//1.4.2.5
		else if(contains(p1,28,14,35) && contains(p2,21,29)){
			return 22;
		}
		//1.4.2.6
		else if(contains(p1,28,14,42) && contains(p2,21,29)){
			return 22;
		}
		////////////////////////////////////////////////////////
		//1.4.3.0
		else if(contains(p1,28,22,0) && contains(p2,21,23)){
			return 29;
		}
		//1.4.3.1
		else if(contains(p1,28,22,7) && contains(p2,21,23)){
			return 8;
		}
		//1.4.3.2
		else if(contains(p1,28,22,14) && contains(p2,21,23)){
			return 29;
		}
		//1.4.3.3
		else if(contains(p1,28,22,24) && contains(p2,21,23)){
			return 25;
		}
		//1.4.3.4
		else if(contains(p1,28,22,29) && contains(p2,21,23)){
			return 30;
		}
		//1.4.3.5
		else if(contains(p1,28,22,35) && contains(p2,21,23)){
			return 29;
		}
		//1.4.3.6
		else if(contains(p1,28,22,42) && contains(p2,21,23)){
			return 29;
		}
		////////////////////////////////////////////////////////
		//1.4.4.0
		else if(contains(p1,28,29,0) && contains(p2,21,30)){
			return 22;
		}
		//1.4.4.1
		else if(contains(p1,28,29,7) && contains(p2,21,30)){
			return 8;
		}
		//1.4.4.2
		else if(contains(p1,28,29,14) && contains(p2,21,30)){
			return 15;
		}
		//1.4.4.3
		else if(contains(p1,28,29,22) && contains(p2,21,30)){
			return 23;
		}
		//1.4.4.4
		else if(contains(p1,28,29,31) && contains(p2,21,30)){
			return 32;
		}
		//1.4.4.5
		else if(contains(p1,28,29,35) && contains(p2,21,30)){
			return 36;
		}
		//1.4.4.6
		else if(contains(p1,28,29,42) && contains(p2,21,30)){
			return 43;
		}
		///////////////////////////////////////////////////////////
		//1.4.5.0
		else if(contains(p1,28,35,0) && contains(p2,21,22)){
			return 29;
		}
		//1.4.5.1
		else if(contains(p1,28,35,7) && contains(p2,21,22)){
			return 29;
		}
		//1.4.5.2
		else if(contains(p1,28,35,14) && contains(p2,21,22)){
			return 15;
		}
		//1.4.5.3
		else if(contains(p1,28,35,23) && contains(p2,21,22)){
			return 24;
		}
		//1.4.5.4
		else if(contains(p1,28,35,29) && contains(p2,21,22)){
			return 30;
		}
		//1.4.5.5
		else if(contains(p1,28,35,36) && contains(p2,21,22)){
			return 37;
		}
		//1.4.5.6
		else if(contains(p1,28,35,42) && contains(p2,21,22)){
			return 29;
		}
		///////////////////////////////////////////////////////////
		//1.4.6.0
		else if(contains(p1,28,42,0) && contains(p2,21,29)){
			return 22;
		}
		//1.4.6.1
		else if(contains(p1,28,42,7) && contains(p2,21,29)){
			return 22;
		}
		//1.4.6.2
		else if(contains(p1,28,42,14) && contains(p2,21,29)){
			return 22;
		}
		//1.4.6.3
		else if(contains(p1,28,42,22) && contains(p2,21,29)){
			return 23;
		}
		//1.4.6.4
		else if(contains(p1,28,42,30) && contains(p2,21,29)){
			return 31;
		}
		//1.4.6.5
		else if(contains(p1,28,42,35) && contains(p2,21,29)){
			return 36;
		}
		//1.4.6.6
		else if(contains(p1,28,42,43) && contains(p2,21,29)){
			return 44;
		}
		//////////////////////////////////////////////////////////
		//1.5.0.0
		else if(contains(p1,35,0,1) && contains(p2,28,7)){
			return 29;
		}
		//1.5.0.1
		else if(contains(p1,35,0,8) && contains(p2,28,7)){
			return 9;
		}
		//1.5.0.2
		else if(contains(p1,35,0,14) && contains(p2,28,7)){
			return 15;
		}
		//1.5.0.3
		else if(contains(p1,35,0,21) && contains(p2,28,7)){
			return 22;
		}
		//1.5.0.4
		else if(contains(p1,35,0,29) && contains(p2,28,7)){
			return 30;
		}
		//1.5.0.5
		else if(contains(p1,35,0,36) && contains(p2,28,7)){
			return 37;
		}
		//1.5.0.6
		else if(contains(p1,35,0,42) && contains(p2,28,7)){
			return 29;
		}
		////////////////////////////////////////////////////
		//1.5.1.0
		else if(contains(p1,35,7,1) && contains(p2,28,0)){
			return 29;
		}
		//1.5.1.1
		else if(contains(p1,35,7,8) && contains(p2,28,0)){
			return 9;
		}
		//1.5.1.2
		else if(contains(p1,35,7,14) && contains(p2,28,0)){
			return 15;
		}
		//1.5.1.3
		else if(contains(p1,35,7,21) && contains(p2,28,0)){
			return 22;
		}
		//1.5.1.4
		else if(contains(p1,35,7,29) && contains(p2,28,0)){
			return 30;
		}
		//1.5.1.5
		else if(contains(p1,35,7,36) && contains(p2,28,0)){
			return 37;
		}
		//1.5.1.6
		else if(contains(p1,35,7,42) && contains(p2,28,0)){
			return 29;
		}
		/////////////////////////////////////////////////////////
		//1.5.2.0
		else if(contains(p1,35,14,0) && contains(p2,28,15)){
			return 7;
		}
		//1.5.2.1
		else if(contains(p1,35,14,7) && contains(p2,28,15)){
			return 0;
		}
		//1.5.2.2
		else if(contains(p1,35,14,16) && contains(p2,28,15)){
			return 17;
		}
		//1.5.2.3
		else if(contains(p1,35,14,21) && contains(p2,28,15)){
			return 22;
		}
		//1.5.2.4
		else if(contains(p1,35,14,29) && contains(p2,28,15)){
			return 30;
		}
		//1.5.2.5
		else if(contains(p1,35,14,36) && contains(p2,28,15)){
			return 37;
		}
		//1.5.2.6
		else if(contains(p1,35,14,42) && contains(p2,28,15)){
			return 29;
		}
		//////////////////////////////////////////////////////////
		//1.5.3.0
		else if(contains(p1,35,21,0) && contains(p2,28,22)){
			return 7;
		}
		//1.5.3.1
		else if(contains(p1,35,21,7) && contains(p2,28,22)){
			return 0;
		}
		//1.5.3.2
		else if(contains(p1,35,21,14) && contains(p2,28,22)){
			return 15;
		}
		//1.5.3.3
		else if(contains(p1,35,21,23) && contains(p2,28,22)){
			return 24;
		}
		//1.5.3.4
		else if(contains(p1,35,21,29) && contains(p2,28,22)){
			return 23;
		}
		//1.5.3.5
		else if(contains(p1,35,21,36) && contains(p2,28,22)){
			return 37;
		}
		//1.5.3.6
		else if(contains(p1,35,21,42) && contains(p2,28,22)){
			return 29;
		}
		/////////////////////////////////////////////////////////
		//1.5.4.0
		else if(contains(p1,35,29,0) && contains(p2,28,30)){
			return 7;
		}
		//1.5.4.1
		else if(contains(p1,35,29,7) && contains(p2,28,30)){
			return 8;
		}
		//1.5.4.2
		else if(contains(p1,35,29,14) && contains(p2,28,30)){
			return 15;
		}
		//1.5.4.3
		else if(contains(p1,35,29,21) && contains(p2,28,30)){
			return 22;
		}
		//1.5.4.4
		else if(contains(p1,35,29,31) && contains(p2,28,30)){
			return 32;
		}
		//1.5.4.5
		else if(contains(p1,35,29,36) && contains(p2,28,30)){
			return 37;
		}
		//1.5.4.6
		else if(contains(p1,35,29,42) && contains(p2,28,30)){
			return 43;
		}
		////////////////////////////////////////////////////////
		//1.5.5.0
		else if(contains(p1,35,36,0) && contains(p2,28,37)){
			return 7;
		}
		//1.5.5.1
		else if(contains(p1,35,36,7) && contains(p2,28,37)){
			return 0;
		}
		//1.5.5.2
		else if(contains(p1,35,36,14) && contains(p2,28,37)){
			return 15;
		}
		//1.5.5.3
		else if(contains(p1,35,36,21) && contains(p2,28,37)){
			return 22;
		}
		//1.5.5.4
		else if(contains(p1,35,36,29) && contains(p2,28,37)){
			return 30;
		}
		//1.5.5.5
		else if(contains(p1,35,36,38) && contains(p2,28,37)){
			return 39;
		}
		//1.5.5.6
		else if(contains(p1,35,36,42) && contains(p2,28,37)){
			return 29;
		}
		///////////////////////////////////////////////////////////
		//1.5.6.0
		else if(contains(p1,35,42,0) && contains(p2,28,29)){
			return 7;
		}
		//1.5.6.1
		else if(contains(p1,35,42,7) && contains(p2,28,29)){
			return 0;
		}
		//1.5.6.2
		else if(contains(p1,35,42,14) && contains(p2,28,29)){
			return 0;
		}
		//1.5.6.3
		else if(contains(p1,35,42,21) && contains(p2,28,29)){
			return 22;
		}
		//1.5.6.4
		else if(contains(p1,35,42,30) && contains(p2,28,29)){
			return 31;
		}
		//1.5.6.5
		else if(contains(p1,35,42,36) && contains(p2,28,29)){
			return 37;
		}
		//1.5.6.6
		else if(contains(p1,35,42,43) && contains(p2,28,29)){
			return 44;
		}
		//////////////////////////////////////////////////////////
		//1.6.0.0
		else if(contains(p1,42,0,2) && contains(p2,21,1)){
			return 14;
		}
		//1.6.0.1
		else if(contains(p1,42,0,7) && contains(p2,21,1)){
			return 22;
		}
		//1.6.0.2
		else if(contains(p1,42,0,14) && contains(p2,21,1)){
			return 15;
		}
		//1.6.0.3
		else if(contains(p1,42,0,22) && contains(p2,21,1)){
			return 23;
		}
		//1.6.0.4
		else if(contains(p1,42,0,28) && contains(p2,21,1)){
			return 29;
		}
		//1.6.0.5
		else if(contains(p1,42,0,35) && contains(p2,21,1)){
			return 36;
		}
		//1.6.0.6
		else if(contains(p1,42,0,43) && contains(p2,21,1)){
			return 44;
		}
		//////////////////////////////////////////////////////
		//1.6.1.0
		else if(contains(p1,42,7,0) && contains(p2,21,22)){
			return 1;
		}
		//1.6.1.1
		else if(contains(p1,42,7,8) && contains(p2,21,22)){
			return 9;
		}
		//1.6.1.2
		else if(contains(p1,42,7,14) && contains(p2,21,22)){
			return 15;
		}
		//1.6.1.3
		else if(contains(p1,42,7,23) && contains(p2,21,22)){
			return 24;
		}
		//1.6.1.4
		else if(contains(p1,42,7,28) && contains(p2,21,22)){
			return 29;
		}
		//1.6.1.5
		else if(contains(p1,42,7,35) && contains(p2,21,22)){
			return 8;
		}
		//1.6.1.6
		else if(contains(p1,42,7,43) && contains(p2,21,22)){
			return 44;
		}
		/////////////////////////////////////////////////////
		//1.6.2.0
		else if(contains(p1,42,14,0) && contains(p2,21,22)){
			return 15;
		}
		//1.6.2.1
		else if(contains(p1,42,14,7) && contains(p2,21,22)){
			return 15;
		}
		//1.6.2.2
		else if(contains(p1,42,14,15) && contains(p2,21,22)){
			return 23;
		}
		//1.6.2.3
		else if(contains(p1,42,14,23) && contains(p2,21,22)){
			return 15;
		}
		//1.6.2.4
		else if(contains(p1,42,14,28) && contains(p2,21,22)){
			return 23;
		}
		//1.6.2.5
		else if(contains(p1,42,14,35) && contains(p2,21,22)){
			return 15;
		}
		//1.6.2.6
		else if(contains(p1,42,14,43) && contains(p2,21,22)){
			return 44;
		}
		///////////////////////////////////////////////////////
		//1.6.3.0
		else if(contains(p1,42,22,0) && contains(p2,21,23)){
			return 24;
		}
		//1.6.3.1
		else if(contains(p1,42,22,7) && contains(p2,21,23)){
			return 24;
		}
		//1.6.3.2
		else if(contains(p1,42,22,14) && contains(p2,21,23)){
			return 15;
		}
		//1.6.3.3
		else if(contains(p1,42,22,24) && contains(p2,21,23)){
			return 25;
		}
		//1.6.3.4
		else if(contains(p1,42,22,28) && contains(p2,21,23)){
			return 29;
		}
		//1.6.3.5
		else if(contains(p1,42,22,35) && contains(p2,21,23)){
			return 36;
		}
		//1.6.3.6
		else if(contains(p1,42,22,43) && contains(p2,21,23)){
			return 44;
		}
		////////////////////////////////////////////////////////
		//1.6.4.0
		else if(contains(p1,42,28,0) && contains(p2,21,29)){
			return 22;
		}
		//1.6.4.1
		else if(contains(p1,42,28,7) && contains(p2,21,29)){
			return 22;
		}
		//1.6.4.2
		else if(contains(p1,42,28,14) && contains(p2,21,29)){
			return 22;
		}
		//1.6.4.3
		else if(contains(p1,42,28,22) && contains(p2,21,29)){
			return 23;
		}
		//1.6.4.4
		else if(contains(p1,42,28,30) && contains(p2,21,29)){
			return 31;
		}
		//1.6.4.5
		else if(contains(p1,42,28,35) && contains(p2,21,29)){
			return 36;
		}
		//1.6.4.6
		else if(contains(p1,42,28,43) && contains(p2,21,29)){
			return 44;
		}
		//////////////////////////////////////////////////////////
		//1.6.5.0
		else if(contains(p1,42,35,0) && contains(p2,21,22)){
			return 1;
		}
		//1.6.5.1
		else if(contains(p1,42,35,7) && contains(p2,21,22)){
			return 8;
		}
		//1.6.5.2
		else if(contains(p1,42,35,14) && contains(p2,21,22)){
			return 15;
		}
		//1.6.5.3
		else if(contains(p1,42,35,23) && contains(p2,21,22)){
			return 24;
		}
		//1.6.5.4
		else if(contains(p1,42,35,28) && contains(p2,21,22)){
			return 29;
		}
		//1.6.5.5
		else if(contains(p1,42,35,36) && contains(p2,21,22)){
			return 37;
		}
		//1.6.5.6
		else if(contains(p1,42,35,43) && contains(p2,21,22)){
			return 44;
		}
		//////////////////////////////////////////////////////////
		//1.6.6.0
		else if(contains(p1,42,43,0) && contains(p2,21,44)){
			return 45;
		}
		//1.6.6.1
		else if(contains(p1,42,43,7) && contains(p2,21,44)){
			return 22;
		}
		//1.6.6.2
		else if(contains(p1,42,43,14) && contains(p2,21,44)){
			return 45;
		}
		//1.6.6.3
		else if(contains(p1,42,43,22) && contains(p2,21,44)){
			return 23;
		}
		//1.6.6.4
		else if(contains(p1,42,43,28) && contains(p2,21,44)){
			return 29;
		}
		//1.6.6.5
		else if(contains(p1,42,43,35) && contains(p2,21,44)){
			return 22;
		}
		//1.6.6.6
		else if(contains(p1,42,43,45) && contains(p2,21,44)){
			return 46;
		}
		///////////////////////////////////////////////////////  miscellaneous
		else if(contains(p1,21,23,28,36) && contains(p2,22,24,35)){
			return 37;
		}
		else if(contains(p1,0,1,3,5) && contains(p2,2,4,21)){
			return 22;
		}
		else if(contains(p1,21,23,25,8) && contains(p2,22,24,7)){
			return 9;
		}
		else if(contains(p1,14,15,22,24) && contains(p2,21,23,16)){
			return 25;
		}
		else if(contains(p1,7,35,43,44) && contains(p2,14,36,42)){
			return 15;
		}
		else if(contains(p1,22,24,42,43) && contains(p2,21,23,44)){
			return 25;
		}
		else if(contains(p1,14,21,23,42) && contains(p2,0,22,28)){
			return 24;
		}
		else if(contains(p1,0,23,25,42) && contains(p2,21,22,24)){
			return 7;
		}
		else if(contains(p1,8,14,21,23) && contains(p2,7,22,24)){
			return 9;
		}
		else if(contains(p1,7,15,17,19) && contains(p2,14,16,18)){
			return 8;
		}
		else if(contains(p1,29,31,33,35) && contains(p2,28,30,32)){
			return 7;
		}
		else if(contains(p1,7,15,17,35) && contains(p2,14,16,42)){
			return 18;
		}
		else if(contains(p1,7,29,31,35) && contains(p2,0,28,30)){
			return 32;
		}
		else if(contains(p1,0,29,31,35) && contains(p2,7,28,30)){
			return 32;
		}
		else if(contains(p1,7,15,17,42) && contains(p2,14,16,35)){
			return 18;
		}
		else if(contains(p1,22,24,28,29) && contains(p2,21,23,25)){
			return 7;
		}
		else if(contains(p1,22,24,14,15) && contains(p2,21,23,25)){
			return 35;
		}
		else if(contains(p1,7,21,23,25) && contains(p2,14,22,24)){
			return 8;
		}
		else if(contains(p1,35,21,23,25) && contains(p2,28,22,24)){
			return 36;
		}
		else if(contains(p1,14,42,23,25) && contains(p2,21,22,24)){
			return 35;
		}
		else{
			return -1;
		}
	}

}
