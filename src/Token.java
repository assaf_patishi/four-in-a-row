import java.awt.Color;
import java.awt.Graphics;



///This class represents a game Token

public class Token {
	
	private int xPos;
	private int yPos;
	private int color;   // 0 for red, 1 for yellow
	
	
	
	public Token(int xPos,int color){
		setXpos(xPos);
		setYpos(0);
		setColor(color);
	}
	
	
	
	public void setXpos(int x){
		xPos = x;
	}
	
	
	
	public int getXpos(){
		return xPos;
	}
	
	
	
	public void setYpos(int y){
		yPos = y;
	}
	
	
	
	 public int getYpos(){
		 return yPos;
	 }
	 
	 
	 
	 public void setColor(int color){
		 this.color = color;
	 }
	 
	 
	 
	 public int getColor(){
		 return color;
	 }

	
	
	
	 public void paintToken(Graphics g){
		if(color == 0){
			g.setColor(Color.RED);
		}
		else if(color == 1){
			g.setColor(Color.YELLOW);
		}
		g.fillOval(xPos, yPos, 85, 85);
	}
	

}
