import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

//The Main class !
public class MyApplication extends JFrame implements ActionListener,ItemListener {
	
	private MyTopJPanel panel;
	private JMenuBar menuBar;
	private JMenu gameOptions;
	private JMenu settings;
	public static JMenuItem resign;
	public static JMenuItem startGame;
	public static JMenuItem undo;
	private JMenu level;
	private JMenu play;
	private ButtonGroup levelGroup;
	public static JRadioButtonMenuItem easy;
	public static JRadioButtonMenuItem intermediate;
	public static JRadioButtonMenuItem hard;
	private ButtonGroup playGroup;
	public static JRadioButtonMenuItem first;
	public static JRadioButtonMenuItem second;
	public static JCheckBoxMenuItem sound;
	
	
	public static void main(String[] args) {
		MyApplication app = new MyApplication();
		app.createGUI();
		app.panel.initComponents();
	}
	
	
	
	public static void resetMenuButtons(){
		startGame.setEnabled(true);
		resign.setEnabled(false);
		first.setEnabled(true);
		second.setEnabled(true);
		easy.setEnabled(true);
		intermediate.setEnabled(true);
		hard.setEnabled(true);
	}
	
	
	
	private void createGUI(){
		panel = new MyTopJPanel();
		setContentPane(panel);
		menuBar = new JMenuBar();
		gameOptions = new JMenu("Game Options");
		settings = new JMenu("Settings");
		level = new JMenu("Level");
		play = new JMenu("Play");
		sound = new JCheckBoxMenuItem("Sound");
		sound.setSelected(true);
		sound.addItemListener(this);
		resign = new JMenuItem("Abort",new ImageIcon(getClass().getResource("/resources/stop.png")));
		resign.setActionCommand("resign");
		resign.addActionListener(this);
		resign.setEnabled(false);
		startGame = new JMenuItem("Start New",new ImageIcon(getClass().getResource("/resources/play.png")));
		startGame.setActionCommand("startGame");
		startGame.addActionListener(this);
		undo = new JMenuItem("Undo");
		undo.setActionCommand("undo");
		undo.addActionListener(this);
		undo.setEnabled(false);
		gameOptions.add(resign);
		gameOptions.add(startGame);
		gameOptions.add(undo);
		settings.add(play);
		settings.add(level);
		settings.add(sound);
		levelGroup = new ButtonGroup();
		playGroup = new ButtonGroup();
		easy = new JRadioButtonMenuItem("Easy");
		easy.setActionCommand("easy");
		easy.addActionListener(this);
		intermediate = new JRadioButtonMenuItem("Intermediate");
		intermediate.setActionCommand("intermediate");
		intermediate.addActionListener(this);
		intermediate.setSelected(true);
		hard = new JRadioButtonMenuItem("Hard");
		hard.setActionCommand("hard");
		hard.addActionListener(this);
		first = new JRadioButtonMenuItem("First");
		first.setActionCommand("first");
		first.addActionListener(this);
		first.setSelected(true);
		second = new JRadioButtonMenuItem("Second");
		second.setActionCommand("second");
		second.addActionListener(this);
		levelGroup.add(easy);
		levelGroup.add(intermediate);
		levelGroup.add(hard);
		playGroup.add(first);
		playGroup.add(second);
		level.add(easy);
		level.add(intermediate);
		level.add(hard);
		play.add(first);
		play.add(second);
		menuBar.add(gameOptions);
		menuBar.add(settings);
		setJMenuBar(menuBar);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800,755);
		setResizable(false);
		setVisible(true);
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		String s = e.getActionCommand();
		if( s.equals("startGame")){
			resign.setEnabled(true);
			startGame.setEnabled(false);
			undo.setEnabled(true);
			panel.playing = true;
			Engine.interrupted = false;
			panel.gameAborted.setVisible(false);
			easy.setEnabled(false);
			intermediate.setEnabled(false);
			hard.setEnabled(false);
			first.setEnabled(false);
			second.setEnabled(false);
			panel.startNewGame();
		}
		else if(s.equals("resign")){
			startGame.setEnabled(true);
			resign.setEnabled(false);
			undo.setEnabled(false);
			panel.playing = false;
			Engine.interrupted = true;
			panel.gameAborted.setVisible(true);
			easy.setEnabled(true);
			intermediate.setEnabled(true);
			hard.setEnabled(true);
			first.setEnabled(true);
			second.setEnabled(true);
		}
		else if(s.equals("undo")){
			panel.undoMove();
		}
		else if(s.equals("first")){
			panel.setEngineNumber(1);
		}
		else if(s.equals("second")){
			panel.setEngineNumber(0);
		}
		else if(s.equals("intermediate")){
			MyTopJPanel.MAX_DEPTH = 14;
		}
		else if(s.equals("easy")){
			MyTopJPanel.MAX_DEPTH = 10;
		}
		else if(s.equals("hard")){
			MyTopJPanel.MAX_DEPTH = 23;
		}

	}



	@Override
	public void itemStateChanged(ItemEvent e) {

		
	}

}
