import java.util.Random;


public class Board {
	
	public long[] board;  // white and black bitBoards
	public int[] height;  // holds the next available open square in column
	public int ply;       
	public long zobristKey;//hashCode for storing positions in the Transposition table (Hash table)
	private long[][] zobristTable; //holds random long number for each legal square on the board
	public int engineNumber;
	public int opponentNumber;
	public KillerMoves km; // The killer moves are used for move ordering in order to speed up the alpha beta algorithm
	private int[] columns = {3,2,4,1,5,0,6}; //static ordering of the columns from the center and out (The center columns should be played in first)
	//private int[] columns2 = {1,4,5,2,3,6,0};
	private int[] columns2 = {5,4,1,2,3,6,0};
	private int[] columns3 = {4,1,2,5,3,6,0};
	private int[] columns4 = {1,3,2,4,5,0,6};
	private int[] columns5 = {2,3,1,5,4,0,6};
	private int[] columns6 = {3,5,2,1,4,6,0};
	private int[] oddSquares = {0,7,14,21,28,35,42,2,9,16,23,30,37,44,4,11,18,25,32,39,46}; 
	private int[] evenSquares = {1,8,15,22,29,36,43,3,10,17,24,31,38,45,5,12,19,26,33,40,47};
	
	
	
	public Board(){
		board = new long[2];
		height = new int[7];
		zobristTable = new long[2][49];
		initZobristTable();
		km = new KillerMoves();
		
	}
	
	
	
	
	public void reset(){
		board[0] = board[1] = 0L;
		ply = 0;
		zobristKey = 0L;
		for(int i = 0; i<7; i++){
			height[i] = i*7;
		}
	}
	
	
	
	private void initZobristTable(){
		Random r = new Random();
		for(int i = 0; i<zobristTable.length; i++){
			for(int j = 0; j<zobristTable[i].length; j++){
				do{
					zobristTable[i][j] = r.nextLong();
				}
				while(zobristTable[i][j]<0);
			}
		}
	}
	
	
	
	
	public void setEngineNumber(int n){
		engineNumber = n;
		opponentNumber = (n==0)? 1 : 0;
	}
	
	
	
	
	public void makeMove(int col){
		board[ply&1] ^= 1L<<height[col];
		zobristKey ^= zobristTable[ply&1][height[col]];
		height[col]++;
		ply++;
	}
	
	
	
	public void unmakeMove(int col){
			height[col]--;
			ply--;
			board[ply&1] ^= 1L<<height[col];
			zobristKey ^= zobristTable[ply&1][height[col]];
	}
	
	
	
	public boolean checkFullColumn(int col){
		return height[col] == col*7+6;
	}
	
	
	
	public boolean checkFullBoard(){
		return (board[0] ^ board[1]) == 279258638311359L;
	}
	
	
	
	
	public boolean checkWin(long newBoard){
		long y;
		y = newBoard & (newBoard>>6);
		if((y & (y>>12)) != 0){  //check diagonal \
			return true;
		}
		y = newBoard & (newBoard>>7);  //check horizontal -
		if((y & (y>>14)) != 0){
			return true;
		}
		y = newBoard & (newBoard>>8);
		if((y & (y>>16)) != 0){         // check diagonal /
			return true;
		}
		y = newBoard & (newBoard>>1);   //check vertical |
		return (y & (y>>2)) != 0;
	}
	
	
	
	
	public int getColumnByIndex(int index){
		if(index>=0 && index<=5){
			return 0;
		}
		else if(index>=7 && index<=12){
			return 1;
		}
		else if(index>=14 && index<=19){
			return 2;
		}
		else if(index>=21 && index<=26){
			return 3;
		}
		else if(index>=28 && index<=33){
			return 4;
		}
		else if(index>=35 && index<=40){
			return 5;
		}
		else if(index>=42 && index<=47){
			return 6;
		}
		else{
			return -1;
		}
	}

	
	
	
	
	public int[] getMoves(int depth){    //returns an array of moves for the alpha beta algorithm
		int[] moves = new int[7];             // if a certain columns is full,the index in the array is set to -1
		int killer1 = km.getKiller(depth, 0);
		int killer2 = km.getKiller(depth, 1);
		int col1 = getColumnByIndex(killer1);
		int col2 = getColumnByIndex(killer2);
		boolean killer1Added = false;
		boolean killer2Added = false;
		int counter = 0;
		if(col1 != -1 && height[col1] == killer1){
			moves[counter] = col1;
			counter++;
			killer1Added = true;
		}
		if(col2 != -1 && height[col2] == killer2){
			moves[counter] = col2;
			counter++;
			killer2Added = true;
		}
		int[] c;
		if(height[3] > 25){
			c = columns2;
		}
		else{
			c = columns;
		}
		for(int i = 0; i<c.length; i++){
			if(killer1Added && c[i] == col1){
				continue;
			}
			if(killer2Added && c[i] == col2){
				continue;
			}
			if(checkFullColumn(c[i])){
				moves[counter] = -1;
				counter++;
				continue;
			}
			moves[counter] = c[i];
			counter++;
		}
		return moves;
	}
	
	
	
	
	public int[] getMoves2(int depth){    //like getMoves()  only with move ordering randomization
		int[] moves = new int[7];             
		int killer1 = km.getKiller(depth, 0);
		int killer2 = km.getKiller(depth, 1);
		int col1 = getColumnByIndex(killer1);
		int col2 = getColumnByIndex(killer2);
		boolean killer1Added = false;
		boolean killer2Added = false;
		int counter = 0;
		if(col1 != -1 && height[col1] == killer1){
			moves[counter] = col1;
			counter++;
			killer1Added = true;
		}
		if(col2 != -1 && height[col2] == killer2){
			moves[counter] = col2;
			counter++;
			killer2Added = true;
		}
		int[] c;
		Random r = new Random();
		int n = r.nextInt(6);
		switch(n){
		case 0:
			c = columns;
			break;
		case 1:
			c = columns2;
			break;
		case 2:
			c = columns3;
			break;
		case 3:
			c = columns4;
			break;
		case 4:
			c = columns5;
			break;
		case 5:
			c = columns6;
			break;
			default:
				c = columns;
		}
		for(int i = 0; i<c.length; i++){
			if(killer1Added && c[i] == col1){
				continue;
			}
			if(killer2Added && c[i] == col2){
				continue;
			}
			if(checkFullColumn(c[i])){
				moves[counter] = -1;
				counter++;
				continue;
			}
			moves[counter] = c[i];
			counter++;
		}
		return moves;
	}
	
	
	
	
	public int[] getMoves3(int depth){   
		int[] moves = new int[7];         
		int killer1 = km.getKiller(depth, 0);
		int killer2 = km.getKiller(depth, 1);
		int col1 = getColumnByIndex(killer1);
		int col2 = getColumnByIndex(killer2);
		boolean killer1Added = false;
		boolean killer2Added = false;
		int counter = 0;
		if(col1 != -1 && height[col1] == killer1){
			moves[counter] = col1;
			counter++;
			killer1Added = true;
		}
		if(col2 != -1 && height[col2] == killer2){
			moves[counter] = col2;
			counter++;
			killer2Added = true;
		}

		for(int i = 0; i<7; i++){
			if(killer1Added && i == col1){
				continue;
			}
			if(killer2Added && i == col2){
				continue;
			}
			if(checkFullColumn(i)){
				moves[counter] = -1;
				counter++;
				continue;
			}
			moves[counter] = i;
			counter++;
		}
		return moves;
	}
	
	

	
	
	
	


	
	
	

	
	
	public int evaluate(){       //simple position Evaluation function ,only looks for one,two and three in a row
		long p1 = board[engineNumber];  // not giving extra points to odd and even threats
		long p2 = board[opponentNumber];
		int c1;
		int c2;
		int enOne = 0;
		int enTwo = 0;
		int enThree = 0;
		int opOne = 0;
		int opTwo = 0;
		int opThree = 0;
		
		//vertical lines
		for(int i = 0; i<=42; i+=7){
			for(int j = i; j<=i+2; j++){
				c1 = Long.bitCount((p1 >> j) & 15L);
				c2 = Long.bitCount((p2 >> j) & 15L);
				if(c1 > 0 && c2 == 0){
					if(c1 == 1){
						enOne++;
					}
					else if(c1 == 2){
						enTwo++;
					}
					else if(c1 == 3){
						enThree++;
					}
				}
				else if(c1 == 0 && c2 > 0){
					if(c2 == 1){
						opOne++;
					}
					else if(c2 == 2){
						opTwo++;
					}
					else if(c2 == 3){
						opThree++;
					}
				}
			}
		}
		// horizontal lines
		for(int i = 0; i<=5; i++){
			for(int j = i; j<=i+21; j+=7){
				c1 = Long.bitCount((p1 >> j) & 2113665L);
				c2 = Long.bitCount((p2 >> j) & 2113665L);
				if(c1 > 0 && c2 == 0){
					if(c1 == 1){
						enOne++;
					}
					else if(c1 == 2){
						enTwo++;
					}
					else if(c1 == 3){
						enThree++;
					}
				}
				else if(c1 == 0 && c2 > 0){
					if(c2 == 1){
						opOne++;
					}
					else if(c2 == 2){
						opTwo++;
					}
					else if(c2 == 3){
						opThree++;
					}
				}
			}
		}
		//diagonals
		for(int i = 5; i<=17; i+=6){
			c1 = Long.bitCount((p1 >> i) & 266305L);
			c2 = Long.bitCount((p2 >> i) & 266305L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					enThree++;
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					opThree++;
				}
			}
		}
		
		for(int i = 4; i<=10; i+=6){
			c1 = Long.bitCount((p1 >> i) & 266305L);
			c2 = Long.bitCount((p2 >> i) & 266305L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					enThree++;
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					opThree++;
				}
			}
		}
		
		c1 = Long.bitCount((p1 >> 3) & 266305L);
		c2 = Long.bitCount((p2 >> 3) & 266305L);
		if(c1 > 0 && c2 == 0){
			if(c1 == 1){
				enOne++;
			}
			else if(c1 == 2){
				enTwo++;
			}
			else if(c1 == 3){
				enThree++;
			}
		}
		else if(c1 == 0 && c2 > 0){
			if(c2 == 1){
				opOne++;
			}
			else if(c2 == 2){
				opTwo++;
			}
			else if(c2 == 3){
				opThree++;
			}
		}
		
		for(int i = 12; i<=24; i+=6){
			c1 = Long.bitCount((p1 >> i) & 266305L);
			c2 = Long.bitCount((p2 >> i) & 266305L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					enThree++;
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					opThree++;
				}
			}
		}
		
		for(int i = 19; i<=25; i+=6){
			c1 = Long.bitCount((p1 >> i) & 266305L);
			c2 = Long.bitCount((p2 >> i) & 266305L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					enThree++;
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					opThree++;
				}
			}
		}
		
		c1 = Long.bitCount((p1 >> 26) & 266305L);
		c2 = Long.bitCount((p2 >> 26) & 266305L);
		if(c1 > 0 && c2 == 0){
			if(c1 == 1){
				enOne++;
			}
			else if(c1 == 2){
				enTwo++;
			}
			else if(c1 == 3){
				enThree++;
			}
		}
		else if(c1 == 0 && c2 > 0){
			if(c2 == 1){
				opOne++;
			}
			else if(c2 == 2){
				opTwo++;
			}
			else if(c2 == 3){
				opThree++;
			}
		}
		///////////////////////////////////////////////
		for(int i = 7; i<=23; i+=8){
			c1 = Long.bitCount((p1 >> i) & 16843009L);
			c2 = Long.bitCount((p2 >> i) & 16843009L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					enThree++;
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					opThree++;
				}
			}
		}
		
		for(int i = 14; i<=22; i+=8){
			c1 = Long.bitCount((p1 >> i) & 16843009L);
			c2 = Long.bitCount((p2 >> i) & 16843009L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					enThree++;
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					opThree++;
				}
			}
		}
		
		c1 = Long.bitCount((p1 >> 21) & 16843009L);
		c2 = Long.bitCount((p2 >> 21) & 16843009L);
		if(c1 > 0 && c2 == 0){
			if(c1 == 1){
				enOne++;
			}
			else if(c1 == 2){
				enTwo++;
			}
			else if(c1 == 3){
				enThree++;
			}
		}
		else if(c1 == 0 && c2 > 0){
			if(c2 == 1){
				opOne++;
			}
			else if(c2 == 2){
				opTwo++;
			}
			else if(c2 == 3){
				opThree++;
			}
		}
		
		for(int i = 0; i<=16; i+=8){
			c1 = Long.bitCount((p1 >> i) & 16843009L);
			c2 = Long.bitCount((p2 >> i) & 16843009L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					enThree++;
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					opThree++;
				}
			}
		}
		
		for(int i = 1; i<=9; i+=8){
			c1 = Long.bitCount((p1 >> i) & 16843009L);
			c2 = Long.bitCount((p2 >> i) & 16843009L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					enThree++;
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					opThree++;
				}
			}
		}
		
		c1 = Long.bitCount((p1 >> 2) & 16843009L);
		c2 = Long.bitCount((p2 >> 2) & 16843009L);
		if(c1 > 0 && c2 == 0){
			if(c1 == 1){
				enOne++;
			}
			else if(c1 == 2){
				enTwo++;
			}
			else if(c1 == 3){
				enThree++;
			}
		}
		else if(c1 == 0 && c2 > 0){
			if(c2 == 1){
				opOne++;
			}
			else if(c2 == 2){
				opTwo++;
			}
			else if(c2 == 3){
				opThree++;
			}
		}
		
		return ((enThree*32) + (enTwo*4) + enOne) - ((opThree*32) + (opTwo*4) + opOne);
		
	}
	
	
	
	
	public int evaluate2(){    //more sophisticated evaluation function, giving extra points to odd and even threats
		long p1 = board[engineNumber];
		long p2 = board[opponentNumber];
		int c1;
		int c2;
		int enOne = 0;
		int enTwo = 0;
		int enThreeOdd = 0;
		int enThreeEven = 0;
		int opOne = 0;
		int opTwo = 0;
		int opThreeOdd = 0;
		int opThreeEven = 0;
		
		//vertical lines
		for(int i = 0; i<=42; i+=7){
			for(int j = i; j<=i+2; j++){
				c1 = Long.bitCount((p1 >> j) & 15L);
				c2 = Long.bitCount((p2 >> j) & 15L);
				if(c1 > 0 && c2 == 0){
					if(c1 == 1){
						enOne++;
					}
					else if(c1 == 2){
						enTwo++;
					}
					else if(c1 == 3){
						for(int k = j; k<=j+3; k++){
							if(((p1 >> k) & 1L) == 0){
								if(isOdd(k)){
									enThreeOdd++;
								}
								else if(isEven(k)){
									enThreeEven++;
								}
								break;
							}
						}
					}
				}
				else if(c1 == 0 && c2 > 0){
					if(c2 == 1){
						opOne++;
					}
					else if(c2 == 2){
						opTwo++;
					}
					else if(c2 == 3){
						for(int k = j; k<=j+3; k++){
							if(((p2 >> k) & 1L) == 0){
								if(isOdd(k)){
									opThreeOdd++;
								}
								else if(isEven(k)){
									opThreeEven++;
								}
								break;
							}
						}
					}
				}
			}
		}
		// horizontal lines
		for(int i = 0; i<=5; i++){
			for(int j = i; j<=i+21; j+=7){
				c1 = Long.bitCount((p1 >> j) & 2113665L);
				c2 = Long.bitCount((p2 >> j) & 2113665L);
				if(c1 > 0 && c2 == 0){
					if(c1 == 1){
						enOne++;
					}
					else if(c1 == 2){
						enTwo++;
					}
					else if(c1 == 3){
						for(int k = j; k<=j+21; k+=7){
							if(((p1 >> k) & 1L) == 0){
								if(isOdd(k)){
									enThreeOdd++;
								}
								else if(isEven(k)){
									enThreeEven++;
								}
								break;
							}
						}
					}
				}
				else if(c1 == 0 && c2 > 0){
					if(c2 == 1){
						opOne++;
					}
					else if(c2 == 2){
						opTwo++;
					}
					else if(c2 == 3){
						for(int k = j; k<=j+21; k+=7){
							if(((p2 >> k) & 1L) == 0){
								if(isOdd(k)){
									opThreeOdd++;
								}
								else if(isEven(k)){
									opThreeEven++;
								}
								break;
							}
						}
					}
				}
			}
		}
		//diagonals
		for(int i = 5; i<=17; i+=6){
			c1 = Long.bitCount((p1 >> i) & 266305L);
			c2 = Long.bitCount((p2 >> i) & 266305L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					for(int k = i; k<=i+18; k+=6){
						if(((p1 >> k) & 1L) == 0){
							if(isOdd(k)){
								enThreeOdd++;
							}
							else if(isEven(k)){
								enThreeEven++;
							}
							break;
						}
					}
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					for(int k = i; k<=i+18; k+=6){
						if(((p2 >> k) & 1L) == 0){
							if(isOdd(k)){
								opThreeOdd++;
							}
							else if(isEven(k)){
								opThreeEven++;
							}
							break;
						}
					}
				}
			}
		}
		
		for(int i = 4; i<=10; i+=6){
			c1 = Long.bitCount((p1 >> i) & 266305L);
			c2 = Long.bitCount((p2 >> i) & 266305L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					for(int k = i; k<=i+18; k+=6){
						if(((p1 >> k) & 1L) == 0){
							if(isOdd(k)){
								enThreeOdd++;
							}
							else if(isEven(k)){
								enThreeEven++;
							}
							break;
						}
					}
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					for(int k = i; k<=i+18; k+=6){
						if(((p2 >> k) & 1L) == 0){
							if(isOdd(k)){
								opThreeOdd++;
							}
							else if(isEven(k)){
								opThreeEven++;
							}
							break;
						}
					}
				}
			}
		}
		
		c1 = Long.bitCount((p1 >> 3) & 266305L);
		c2 = Long.bitCount((p2 >> 3) & 266305L);
		if(c1 > 0 && c2 == 0){
			if(c1 == 1){
				enOne++;
			}
			else if(c1 == 2){
				enTwo++;
			}
			else if(c1 == 3){
				for(int k = 3; k<=21; k+=6){
					if(((p1 >> k) & 1L) == 0){
						if(isOdd(k)){
							enThreeOdd++;
						}
						else if(isEven(k)){
							enThreeEven++;
						}
						break;
					}
				}
			}
		}
		else if(c1 == 0 && c2 > 0){
			if(c2 == 1){
				opOne++;
			}
			else if(c2 == 2){
				opTwo++;
			}
			else if(c2 == 3){
				for(int k = 3; k<=21; k+=6){
					if(((p2 >> k) & 1L) == 0){
						if(isOdd(k)){
							opThreeOdd++;
						}
						else if(isEven(k)){
							opThreeEven++;
						}
						break;
					}
				}
			}
		}
		
		for(int i = 12; i<=24; i+=6){
			c1 = Long.bitCount((p1 >> i) & 266305L);
			c2 = Long.bitCount((p2 >> i) & 266305L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					for(int k = i; k<=i+18; k+=6){
						if(((p1 >> k) & 1L) == 0){
							if(isOdd(k)){
								enThreeOdd++;
							}
							else if(isEven(k)){
								enThreeEven++;
							}
							break;
						}
					}
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					for(int k = i; k<=i+18; k+=6){
						if(((p2 >> k) & 1L) == 0){
							if(isOdd(k)){
								opThreeOdd++;
							}
							else if(isEven(k)){
								opThreeEven++;
							}
							break;
						}
					}
				}
			}
		}
		
		for(int i = 19; i<=25; i+=6){
			c1 = Long.bitCount((p1 >> i) & 266305L);
			c2 = Long.bitCount((p2 >> i) & 266305L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					for(int k = i; k<=i+18; k+=6){
						if(((p1 >> k) & 1L) == 0){
							if(isOdd(k)){
								enThreeOdd++;
							}
							else if(isEven(k)){
								enThreeEven++;
							}
							break;
						}
					}
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					for(int k = i; k<=i+18; k+=6){
						if(((p2 >> k) & 1L) == 0){
							if(isOdd(k)){
								opThreeOdd++;
							}
							else if(isEven(k)){
								opThreeEven++;
							}
							break;
						}
					}
				}
			}
		}
		
		c1 = Long.bitCount((p1 >> 26) & 266305L);
		c2 = Long.bitCount((p2 >> 26) & 266305L);
		if(c1 > 0 && c2 == 0){
			if(c1 == 1){
				enOne++;
			}
			else if(c1 == 2){
				enTwo++;
			}
			else if(c1 == 3){
				for(int k = 26; k<=44; k+=6){
					if(((p1 >> k) & 1L) == 0){
						if(isOdd(k)){
							enThreeOdd++;
						}
						else if(isEven(k)){
							enThreeEven++;
						}
						break;
					}
				}
			}
		}
		else if(c1 == 0 && c2 > 0){
			if(c2 == 1){
				opOne++;
			}
			else if(c2 == 2){
				opTwo++;
			}
			else if(c2 == 3){
				for(int k = 26; k<=44; k+=6){
					if(((p2 >> k) & 1L) == 0){
						if(isOdd(k)){
							opThreeOdd++;
						}
						else if(isEven(k)){
							opThreeEven++;
						}
						break;
					}
				}
			}
		}
		///////////////////////////////////////////////
		for(int i = 7; i<=23; i+=8){
			c1 = Long.bitCount((p1 >> i) & 16843009L);
			c2 = Long.bitCount((p2 >> i) & 16843009L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					for(int k = i; k<=i+24; k+=8){
						if(((p1 >> k) & 1L) == 0){
							if(isOdd(k)){
								enThreeOdd++;
							}
							else if(isEven(k)){
								enThreeEven++;
							}
							break;
						}
					}
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					for(int k = i; k<=i+24; k+=8){
						if(((p2 >> k) & 1L) == 0){
							if(isOdd(k)){
								opThreeOdd++;
							}
							else if(isEven(k)){
								opThreeEven++;
							}
							break;
						}
					}
				}
			}
		}
		
		for(int i = 14; i<=22; i+=8){
			c1 = Long.bitCount((p1 >> i) & 16843009L);
			c2 = Long.bitCount((p2 >> i) & 16843009L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					for(int k = i; k<=i+24; k+=8){
						if(((p1 >> k) & 1L) == 0){
							if(isOdd(k)){
								enThreeOdd++;
							}
							else if(isEven(k)){
								enThreeEven++;
							}
							break;
						}
					}
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					for(int k = i; k<=i+24; k+=8){
						if(((p2 >> k) & 1L) == 0){
							if(isOdd(k)){
								opThreeOdd++;
							}
							else if(isEven(k)){
								opThreeEven++;
							}
							break;
						}
					}
				}
			}
		}
		
		c1 = Long.bitCount((p1 >> 21) & 16843009L);
		c2 = Long.bitCount((p2 >> 21) & 16843009L);
		if(c1 > 0 && c2 == 0){
			if(c1 == 1){
				enOne++;
			}
			else if(c1 == 2){
				enTwo++;
			}
			else if(c1 == 3){
				for(int k = 21; k<=45; k+=8){
					if(((p1 >> k) & 1L) == 0){
						if(isOdd(k)){
							enThreeOdd++;
						}
						else if(isEven(k)){
							enThreeEven++;
						}
						break;
					}
				}
			}
		}
		else if(c1 == 0 && c2 > 0){
			if(c2 == 1){
				opOne++;
			}
			else if(c2 == 2){
				opTwo++;
			}
			else if(c2 == 3){
				for(int k = 21; k<=45; k+=8){
					if(((p2 >> k) & 1L) == 0){
						if(isOdd(k)){
							opThreeOdd++;
						}
						else if(isEven(k)){
							opThreeEven++;
						}
						break;
					}
				}
			}
		}
		
		for(int i = 0; i<=16; i+=8){
			c1 = Long.bitCount((p1 >> i) & 16843009L);
			c2 = Long.bitCount((p2 >> i) & 16843009L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					for(int k = i; k<=i+24; k+=8){
						if(((p1 >> k) & 1L) == 0){
							if(isOdd(k)){
								enThreeOdd++;
							}
							else if(isEven(k)){
								enThreeEven++;
							}
							break;
						}
					}
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					for(int k = i; k<=i+24; k+=8){
						if(((p2 >> k) & 1L) == 0){
							if(isOdd(k)){
								opThreeOdd++;
							}
							else if(isEven(k)){
								opThreeEven++;
							}
							break;
						}
					}
				}
			}
		}
		
		for(int i = 1; i<=9; i+=8){
			c1 = Long.bitCount((p1 >> i) & 16843009L);
			c2 = Long.bitCount((p2 >> i) & 16843009L);
			if(c1 > 0 && c2 == 0){
				if(c1 == 1){
					enOne++;
				}
				else if(c1 == 2){
					enTwo++;
				}
				else if(c1 == 3){
					for(int k = i; k<=i+24; k+=8){
						if(((p1 >> k) & 1L) == 0){
							if(isOdd(k)){
								enThreeOdd++;
							}
							else if(isEven(k)){
								enThreeEven++;
							}
							break;
						}
					}
				}
			}
			else if(c1 == 0 && c2 > 0){
				if(c2 == 1){
					opOne++;
				}
				else if(c2 == 2){
					opTwo++;
				}
				else if(c2 == 3){
					for(int k = i; k<=i+24; k+=8){
						if(((p2 >> k) & 1L) == 0){
							if(isOdd(k)){
								opThreeOdd++;
							}
							else if(isEven(k)){
								opThreeEven++;
							}
							break;
						}
					}
				}
			}
		}
		
		c1 = Long.bitCount((p1 >> 2) & 16843009L);
		c2 = Long.bitCount((p2 >> 2) & 16843009L);
		if(c1 > 0 && c2 == 0){
			if(c1 == 1){
				enOne++;
			}
			else if(c1 == 2){
				enTwo++;
			}
			else if(c1 == 3){
				for(int k = 2; k<=26; k+=8){
					if(((p1 >> k) & 1L) == 0){
						if(isOdd(k)){
							enThreeOdd++;
						}
						else if(isEven(k)){
							enThreeEven++;
						}
						break;
					}
				}
			}
		}
		else if(c1 == 0 && c2 > 0){
			if(c2 == 1){
				opOne++;
			}
			else if(c2 == 2){
				opTwo++;
			}
			else if(c2 == 3){
				for(int k = 2; k<=26; k+=8){
					if(((p2 >> k) & 1L) == 0){
						if(isOdd(k)){
							opThreeOdd++;
						}
						else if(isEven(k)){
							opThreeEven++;
						}
						break;
					}
				}
			}
		}
		
		if(engineNumber == 0){
			return ((enThreeOdd*32) + (enThreeEven*16) + (enTwo*4) + enOne) - ((opThreeEven*32) + (opThreeOdd*16) + (opTwo*4) + opOne);
		}
		else{
			return ((enThreeOdd*16) + (enThreeEven*32) + (enTwo*4) + enOne) - ((opThreeEven*16) + (opThreeOdd*32) + (opTwo*4) + opOne);
		}
		
		
	}
	
	
	
	
	private boolean isOdd(int square){  //return true of the square is Odd (on an odd row of the board)
		for(int s:oddSquares){
			if(square == s){
				return true;
			}
		}
		return false;
	}
	
	
	
	
	private boolean isEven(int square){// returns true if the square is even
		for(int s:evenSquares){
			if(square == s){
				return true;
			}
		}
		return false;
	}
	
	
	
	
	
	public int evaluate3(){  //// this evaluation function is not in use
		long p1 = board[engineNumber];
		long p2 = board[opponentNumber];
		int totalScore = 0;
		int lineScore;
		//vertical lines
		for(int i = 0; i<=42; i+=7){
			for(int j = i; j<=i+2; j++){
				lineScore = 0;
				for(int k = j; k<=j+3; k++){
					if(((p1 >> k) & 1L) != 0){
						lineScore++;
					}
					else if(((p2 >> k) & 1L) != 0){
						lineScore--;
					}
				}
				totalScore += lineScore;
			}
		}
		// horizontal lines
		for(int i = 0; i<=5; i++){
			for(int j = i; j<=i+21; j+=7){
				lineScore = 0;
				for(int k = j; k<=j+21; k+=7){
					if(((p1 >> k) & 1L) != 0){
						lineScore++;
					}
					else if(((p2 >> k) & 1L) != 0){
						lineScore--;
					}
				}
				totalScore += lineScore;
			}
		}
		//diagonals
		for(int i = 5; i<=17; i+=6){
			lineScore = 0;
			for(int j = i; j<=i+18; j+=6){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
			}
			totalScore += lineScore;
		}
		
		for(int i = 4; i<=10; i+=6){
			lineScore = 0;
			for(int j = i; j<=i+18; j+=6){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
			}
			totalScore += lineScore;
		}
		
		lineScore = 0;
		for(int j = 3; j<=21; j+=6){
			if(((p1 >> j) & 1L) != 0){
				lineScore++;
			}
			else if(((p2 >> j) & 1L) != 0){
				lineScore--;
			}
		}
		totalScore += lineScore;
		
		for(int i = 12; i<=24; i+=6){
			lineScore = 0;
			for(int j = i; j<=i+18; j+=6){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
			}
			totalScore += lineScore;
		}
		
		for(int i = 19; i<=25; i+=6){
			lineScore = 0;
			for(int j = i; j<=i+18; j+=6){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
			}
			totalScore += lineScore;
		}
		
		lineScore = 0;
		for(int j = 26; j<=44; j+=6){
			if(((p1 >> j) & 1L) != 0){
				lineScore++;
			}
			else if(((p2 >> j) & 1L) != 0){
				lineScore--;
			}
		}
		totalScore += lineScore;
		
		for(int i = 7; i<=23; i+=8){
			lineScore = 0;
			for(int j = i; j<=i+24; j+=8){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
			}
			totalScore += lineScore;
		}
		
		for(int i = 14; i<=22; i+=8){
			lineScore = 0;
			for(int j = i; j<=i+24; j+=8){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
			}
			totalScore += lineScore;
		}
		
		lineScore = 0;
		for(int j = 21; j<=45; j+=8){
			if(((p1 >> j) & 1L) != 0){
				lineScore++;
			}
			else if(((p2 >> j) & 1L) != 0){
				lineScore--;
			}
		}
		totalScore += lineScore;
		
		for(int i = 0; i<=16; i+=8){
			lineScore = 0;
			for(int j = i; j<=i+24; j+=8){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
			}
			totalScore += lineScore;
		}
		
		for(int i = 1; i<=9; i+=8){
			lineScore = 0;
			for(int j = i; j<=i+24; j+=8){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
			}
			totalScore += lineScore;
		}
		
		lineScore = 0;
		for(int j = 2; j<=26; j+=8){
			if(((p1 >> j) & 1L) != 0){
				lineScore++;
			}
			else if(((p2 >> j) & 1L) != 0){
				lineScore--;
			}
		}
		totalScore += lineScore;
		
		return totalScore;
		
		
	}
	
	
	
	
	public int[] getCoordinatesByIndex(int index){  //returns the x and y coordinates for the moves animation
		int[] coordinates = new int[2];
		switch(index){
		case 5:
			coordinates[0] = 56;
			coordinates[1] = 58;
			break;
		case 4:
			coordinates[0] = 56;
			coordinates[1] = 158;
			break;
		case 3:
			coordinates[0] = 56;
			coordinates[1] = 258;
			break;
		case 2:
			coordinates[0] = 56;
			coordinates[1] = 358;
			break;
		case 1:
			coordinates[0] = 56;
			coordinates[1] = 458;
			break;
		case 0:
			coordinates[0] = 56;
			coordinates[1] = 558;
			break;
		case 12:
			coordinates[0] = 156;
			coordinates[1] = 58;
			break;
		case 11:
			coordinates[0] = 156;
			coordinates[1] = 158;
			break;
		case 10:
			coordinates[0] = 156;
			coordinates[1] = 258;
			break;
		case 9:
			coordinates[0] = 156;
			coordinates[1] = 358;
			break;
		case 8:
			coordinates[0] = 156;
			coordinates[1] = 458;
			break;
		case 7:
			coordinates[0] = 156;
			coordinates[1] = 558;
			break;
		case 19:
			coordinates[0] = 256;
			coordinates[1] = 58;
			break;
		case 18:
			coordinates[0] = 256;
			coordinates[1] = 158;
			break;
		case 17:
			coordinates[0] = 256;
			coordinates[1] = 258;
			break;
		case 16:
			coordinates[0] = 256;
			coordinates[1] = 358;
			break;
		case 15:
			coordinates[0] = 256;
			coordinates[1] = 458;
			break;
		case 14:
			coordinates[0] = 256;
			coordinates[1] = 558;
			break;
		case 26:
			coordinates[0] = 356;
			coordinates[1] = 58;
			break;
		case 25:
			coordinates[0] = 356;
			coordinates[1] = 158;
			break;
		case 24:
			coordinates[0] = 356;
			coordinates[1] = 258;
			break;
		case 23:
			coordinates[0] = 356;
			coordinates[1] = 358;
			break;
		case 22:
			coordinates[0] = 356;
			coordinates[1] = 458;
			break;
		case 21:
			coordinates[0] = 356;
			coordinates[1] = 558;
			break;
		case 33:
			coordinates[0] = 456;
			coordinates[1] = 58;
			break;
		case 32:
			coordinates[0] = 456;
			coordinates[1] = 158;
			break;
		case 31:
			coordinates[0] = 456;
			coordinates[1] = 258;
			break;
		case 30:
			coordinates[0] = 456;
			coordinates[1] = 358;
			break;
		case 29:
			coordinates[0] = 456;
			coordinates[1] = 458;
			break;
		case 28:
			coordinates[0] = 456;
			coordinates[1] = 558;
			break;
		case 40:
			coordinates[0] = 556;
			coordinates[1] = 58;
			break;
		case 39:
			coordinates[0] = 556;
			coordinates[1] = 158;
			break;
		case 38:
			coordinates[0] = 556;
			coordinates[1] = 258;
			break;
		case 37:
			coordinates[0] = 556;
			coordinates[1] = 358;
			break;
		case 36:
			coordinates[0] = 556;
			coordinates[1] = 458;
			break;
		case 35:
			coordinates[0] = 556;
			coordinates[1] = 558;
			break;
		case 47:
			coordinates[0] = 656;
			coordinates[1] = 58;
			break;
		case 46:
			coordinates[0] = 656;
			coordinates[1] = 158;
			break;
		case 45:
			coordinates[0] = 656;
			coordinates[1] = 258;
			break;
		case 44:
			coordinates[0] = 656;
			coordinates[1] = 358;
			break;
		case 43:
			coordinates[0] = 656;
			coordinates[1] = 458;
			break;
		case 42:
			coordinates[0] = 656;
			coordinates[1] = 558;
			break;
		}
		return coordinates;
	}
	
	
	
	//return the start and end points of the winning line for the green line animation
	public int[] getWinningIndexes(long newBoard){  //0 = horizontal, 1 = diagonal \, 2 = diagonal / , 3 = vertical
		int[] winningIndexes = new int[3];
		//horizontal lines
		for(int i = 0; i<=5; i++){
			for(int j = i; j<=i+21; j+=7){
				if(((newBoard >> j) & 2113665L) == 2113665L){
					winningIndexes[0] = j;
					//winningIndexes[1] = j+7;
					//winningIndexes[2] = j+14;
					winningIndexes[1] = j+21;
					winningIndexes[2] = 0;
					return winningIndexes;
				}
			}
		}
		//diagonals
		for(int i = 5; i<=17; i+=6){
			if(((newBoard >> i) & 266305L) == 266305L){
				winningIndexes[0] = i;
				//winningIndexes[1] = i+6;
				//winningIndexes[2] = i+12;
				winningIndexes[1] = i+18;
				winningIndexes[2] = 1;
				return winningIndexes;
			}
		}
		
		for(int i = 4; i<=10; i+=6){
			if(((newBoard >> i) & 266305L) == 266305L){
				winningIndexes[0] = i;
				//winningIndexes[1] = i+6;
				//winningIndexes[2] = i+12;
				winningIndexes[1] = i+18;
				winningIndexes[2] = 1;
				return winningIndexes;
			}
		}
		
		if(((newBoard >> 3) & 266305L) == 266305L){
			winningIndexes[0] = 3;
			//winningIndexes[1] = 9;
			//winningIndexes[2] = 15;
			winningIndexes[1] = 21;
			winningIndexes[2] = 1;
			return winningIndexes;
		}
		
		for(int i = 12; i<=24; i+=6){
			if(((newBoard >> i) & 266305L) == 266305L){
				winningIndexes[0] = i;
				//winningIndexes[1] = i+6;
				//winningIndexes[2] = i+12;
				winningIndexes[1] = i+18;
				winningIndexes[2] = 1;
				return winningIndexes;
			}
		}
		
		
		for(int i = 19; i<=25; i+=6){
			if(((newBoard >> i) & 266305L) == 266305L){
				winningIndexes[0] = i;
				//winningIndexes[1] = i+6;
				//winningIndexes[2] = i+12;
				winningIndexes[1] = i+18;
				winningIndexes[2] = 1;
				return winningIndexes;
			}
		}
		
		if(((newBoard >> 26) & 266305L) == 266305L){
			winningIndexes[0] = 26;
			//winningIndexes[1] = 32;
			//winningIndexes[2] = 38;
			winningIndexes[1] = 44;
			winningIndexes[2] = 1;
			return winningIndexes;
		}
		
		for(int i = 7; i<=23; i+=8){
			if(((newBoard >> i) & 16843009L) == 16843009L){
				winningIndexes[0] = i;
				//winningIndexes[1] = i+8;
				//winningIndexes[2] = i+16;
				winningIndexes[1] = i+24;
				winningIndexes[2] = 2;
				return winningIndexes;
			}
		}
		
		for(int i = 14; i<=22; i+=8){
			if(((newBoard >> i) & 16843009L) == 16843009L){
				winningIndexes[0] = i;
				//winningIndexes[1] = i+8;
				//winningIndexes[2] = i+16;
				winningIndexes[1] = i+24;
				winningIndexes[2] = 2;
				return winningIndexes;
			}
		}
		
		if(((newBoard >> 21) & 16843009L) == 16843009L){
			winningIndexes[0] = 21;
			//winningIndexes[1] = 29;
			//winningIndexes[2] = 37;
			winningIndexes[1] = 45;
			winningIndexes[2] = 2;
			return winningIndexes;
		}
		
		for(int i = 0; i<=16; i+=8){
			if(((newBoard >> i) & 16843009L) == 16843009L){
				winningIndexes[0] = i;
				//winningIndexes[1] = i+8;
				//winningIndexes[2] = i+16;
				winningIndexes[1] = i+24;
				winningIndexes[2] = 2;
				return winningIndexes;
			}
		}
		
		for(int i = 1; i<=9; i+=8){
			if(((newBoard >> i) & 16843009L) == 16843009L){
				winningIndexes[0] = i;
				//winningIndexes[1] = i+8;
				//winningIndexes[2] = i+16;
				winningIndexes[1] = i+24;
				winningIndexes[2] = 2;
				return winningIndexes;
			}
		}
		
		if(((newBoard >> 2) & 16843009L) == 16843009L){
			winningIndexes[0] = 2;
			//winningIndexes[1] = 10;
			//winningIndexes[2] = 18;
			winningIndexes[1] = 26;
			winningIndexes[2] = 2;
			return winningIndexes;
		}
		
		//vertical lines
		for(int i = 0; i<=42; i+=7){
			for(int j = i; j<=i+2; j++){
				if(((newBoard >> j) & 15L) == 15L){
					winningIndexes[0] = j;
					//winningIndexes[1] = j+1;
					//winningIndexes[2] = j+2;
					winningIndexes[1] = j+3;
					winningIndexes[2] = 3;
					return winningIndexes;
				}
			}
		}
		
		return winningIndexes;
	}
	
	
	
	

	
	public int checkImmediateThreat(long newBoard){  //check whether the opponent has an immediate threat
		long p1 = newBoard;
		long p2;
		if(newBoard == board[0]){
			p2 = board[1];
		}
		else{
			p2 = board[0];
		}
		int lineScore;
		int threat = -1;
		//vertical lines
		for(int i = 0; i<=42; i+=7){
			for(int j = i; j<=i+2; j++){
				lineScore = 0;
				for(int k = j; k<=j+3; k++){
					if(((p1 >> k) & 1L) != 0){
						lineScore++;
					}
					else if(((p2 >> k) & 1L) != 0){
						lineScore--;
					}
					else{
						threat = k;
					}
				}
				if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
					return threat;
				}
			}
		}
		// horizontal lines
		for(int i = 0; i<=5; i++){
			for(int j = i; j<=i+21; j+=7){
				lineScore = 0;
				for(int k = j; k<=j+21; k+=7){
					if(((p1 >> k) & 1L) != 0){
						lineScore++;
					}
					else if(((p2 >> k) & 1L) != 0){
						lineScore--;
					}
					else{
						threat = k;
					}
				}
				if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
					return threat;
				}
			}
		}
		//diagonals
		for(int i = 5; i<=17; i+=6){
			lineScore = 0;
			for(int j = i; j<=i+18; j+=6){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
				else{
					threat = j;
				}
			}
			if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
				return threat;
			}
		}
		
		for(int i = 4; i<=10; i+=6){
			lineScore = 0;
			for(int j = i; j<=i+18; j+=6){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
				else{
					threat = j;
				}
			}
			if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
				return threat;
			}
		}
		
		lineScore = 0;
		for(int j = 3; j<=21; j+=6){
			if(((p1 >> j) & 1L) != 0){
				lineScore++;
			}
			else if(((p2 >> j) & 1L) != 0){
				lineScore--;
			}
			else{
				threat = j;
			}
		}
		if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
			return threat;
		}
		
		for(int i = 12; i<=24; i+=6){
			lineScore = 0;
			for(int j = i; j<=i+18; j+=6){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
				else{
					threat = j;
				}
			}
			if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
				return threat;
			}
		}
		
		for(int i = 19; i<=25; i+=6){
			lineScore = 0;
			for(int j = i; j<=i+18; j+=6){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
				else{
					threat = j;
				}
			}
			if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
				return threat;
			}
		}
		
		lineScore = 0;
		for(int j = 26; j<=44; j+=6){
			if(((p1 >> j) & 1L) != 0){
				lineScore++;
			}
			else if(((p2 >> j) & 1L) != 0){
				lineScore--;
			}
			else{
				threat = j;
			}
		}
		if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
			return threat;
		}
		
		for(int i = 7; i<=23; i+=8){
			lineScore = 0;
			for(int j = i; j<=i+24; j+=8){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
				else{
					threat = j;
				}
			}
			if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
				return threat;
			}
		}
		
		for(int i = 14; i<=22; i+=8){
			lineScore = 0;
			for(int j = i; j<=i+24; j+=8){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
				else{
					threat = j;
				}
			}
			if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
				return threat;
			}
		}
		
		lineScore = 0;
		for(int j = 21; j<=45; j+=8){
			if(((p1 >> j) & 1L) != 0){
				lineScore++;
			}
			else if(((p2 >> j) & 1L) != 0){
				lineScore--;
			}
			else{
				threat = j;
			}
		}
		if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
			return threat;
		}
		
		for(int i = 0; i<=16; i+=8){
			lineScore = 0;
			for(int j = i; j<=i+24; j+=8){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
				else{
					threat = j;
				}
			}
			if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
				return threat;
			}
		}
		
		for(int i = 1; i<=9; i+=8){
			lineScore = 0;
			for(int j = i; j<=i+24; j+=8){
				if(((p1 >> j) & 1L) != 0){
					lineScore++;
				}
				else if(((p2 >> j) & 1L) != 0){
					lineScore--;
				}
				else{
					threat = j;
				}
			}
			if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
				return threat;
			}
		}
		
		lineScore = 0;
		for(int j = 2; j<=26; j+=8){
			if(((p1 >> j) & 1L) != 0){
				lineScore++;
			}
			else if(((p2 >> j) & 1L) != 0){
				lineScore--;
			}
			else{
				threat = j;
			}
		}
		if(lineScore == -3 && threat == height[getColumnByIndex(threat)]){
			return threat;
		}
		
		return -1;
		
		
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
