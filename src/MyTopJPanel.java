
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

///////This class holds all the main drawings and animation functionality 

public class MyTopJPanel extends JPanel implements MouseListener{
	
	
	private JLabel label;
	private ImageIcon image;
	private ArrayList<Token>tokenList;
	private ArrayList<Integer>p1Moves;
	private ArrayList<Integer>p2Moves;
	private Board board;
	private Engine engine;
	public static int MAX_DEPTH = 14;
	private boolean engineTurn;
	private boolean engineWin;
	private boolean opponentWin;
	private boolean draw;
	public boolean playing;
	private JLabel winningLabel;
	private JLabel thinkingLabel;
	private int winningPlayer;
	public JLabel gameAborted;
	
	
	public MyTopJPanel(){
		super(new BorderLayout());
		image = new ImageIcon(getClass().getResource("/resources/board2.png"));
		label = new JLabel(image);
		winningLabel = new JLabel();
		gameAborted = new JLabel("Game Aborted");
		gameAborted.setSize(400,70);
		gameAborted.setFont(new Font("Serif", Font.PLAIN, 48));
		gameAborted.setForeground(Color.RED);
		gameAborted.setLocation(265, -12);
		gameAborted.setVisible(false);
		thinkingLabel = new JLabel("Analyzing...");
		thinkingLabel.setSize(400,70);
		thinkingLabel.setFont(new Font("Serif", Font.PLAIN, 48));
		thinkingLabel.setForeground(Color.CYAN);
		thinkingLabel.setLocation(290, -12);
		thinkingLabel.setVisible(false);
		add(label);
		label.add(winningLabel);
		label.add(thinkingLabel);
		label.add(gameAborted);
		addMouseListener(this);
		
	}
	
	
	
	
	public void initComponents(){
		tokenList = new ArrayList<Token>();
		p1Moves = new ArrayList<Integer>();
		p2Moves = new ArrayList<Integer>();
		board = new Board();
		engine = new Engine(board);
		board.setEngineNumber(1);
	}
	
	
	
	public void startNewGame(){
		board.reset();
		tokenList.clear();
		p1Moves.clear();
		p2Moves.clear();
		engineWin = false;
		opponentWin = false;
		draw = false;
		if(MAX_DEPTH > 14){
			engine.tt.initTable();
		}
		winningLabel.setVisible(false);
		repaint();
		if(board.engineNumber == 0){
			engineTurn = true;
			PlayMove p = new PlayMove();
			p.start();
		}
		else{
			engineTurn = false;
		}
	}
	
	
	

	
	public void setEngineNumber(int n){
		board.setEngineNumber(n);
	}

	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(!engineTurn && playing){
			if(e.getX()>=56 && e.getX()<=142){
				int move = 0;
				if(!board.checkFullColumn(move)){
					engineTurn = true;
					if(board.opponentNumber == 0){
						p1Moves.add(board.height[move]);
					}
					else{
						p2Moves.add(board.height[move]);
					}
					int[] coordinates = board.getCoordinatesByIndex(board.height[move]);
					board.makeMove(move);
					tokenList.add(new Token(coordinates[0],board.opponentNumber));
					AnimationThread a = new AnimationThread(coordinates[1],tokenList.get(tokenList.size()-1));
					a.start();
				}
			}
			else if(e.getX()>=156 && e.getX()<=242){
				int move = 1;
				if(!board.checkFullColumn(move)){
					engineTurn = true;
					if(board.opponentNumber == 0){
						p1Moves.add(board.height[move]);
					}
					else{
						p2Moves.add(board.height[move]);
					}
					int[] coordinates = board.getCoordinatesByIndex(board.height[move]);
					board.makeMove(move);
					tokenList.add(new Token(coordinates[0],board.opponentNumber));
					AnimationThread a = new AnimationThread(coordinates[1],tokenList.get(tokenList.size()-1));
					a.start();
				}
			}
			else if(e.getX()>=256 && e.getX()<=342){
				int move = 2;
				if(!board.checkFullColumn(move)){
					engineTurn = true;
					if(board.opponentNumber == 0){
						p1Moves.add(board.height[move]);
					}
					else{
						p2Moves.add(board.height[move]);
					}
					int[] coordinates = board.getCoordinatesByIndex(board.height[move]);
					board.makeMove(move);
					tokenList.add(new Token(coordinates[0],board.opponentNumber));
					AnimationThread a = new AnimationThread(coordinates[1],tokenList.get(tokenList.size()-1));
					a.start();
				}
			}
			else if(e.getX()>=356 && e.getX()<=442){
				int move = 3;
				if(!board.checkFullColumn(move)){
					engineTurn = true;
					if(board.opponentNumber == 0){
						p1Moves.add(board.height[move]);
					}
					else{
						p2Moves.add(board.height[move]);
					}
					int[] coordinates = board.getCoordinatesByIndex(board.height[move]);
					board.makeMove(move);
					tokenList.add(new Token(coordinates[0],board.opponentNumber));
					AnimationThread a = new AnimationThread(coordinates[1],tokenList.get(tokenList.size()-1));
					a.start();
				}
			}
			else if(e.getX()>=456 && e.getX()<=542){
				int move = 4;
				if(!board.checkFullColumn(move)){
					engineTurn = true;
					if(board.opponentNumber == 0){
						p1Moves.add(board.height[move]);
					}
					else{
						p2Moves.add(board.height[move]);
					}
					int[] coordinates = board.getCoordinatesByIndex(board.height[move]);
					board.makeMove(move);
					tokenList.add(new Token(coordinates[0],board.opponentNumber));
					AnimationThread a = new AnimationThread(coordinates[1],tokenList.get(tokenList.size()-1));
					a.start();
				}
			}
			else if(e.getX()>=556 && e.getX()<=642){
				int move = 5;
				if(!board.checkFullColumn(move)){
					engineTurn = true;
					if(board.opponentNumber == 0){
						p1Moves.add(board.height[move]);
					}
					else{
						p2Moves.add(board.height[move]);
					}
					int[] coordinates = board.getCoordinatesByIndex(board.height[move]);
					board.makeMove(move);
					tokenList.add(new Token(coordinates[0],board.opponentNumber));
					AnimationThread a = new AnimationThread(coordinates[1],tokenList.get(tokenList.size()-1));
					a.start();
				}
			}
			else if(e.getX()>=656 && e.getX()<=742){
				int move = 6;
				if(!board.checkFullColumn(move)){
					engineTurn = true;
					if(board.opponentNumber == 0){
						p1Moves.add(board.height[move]);
					}
					else{
						p2Moves.add(board.height[move]);
					}
					int[] coordinates = board.getCoordinatesByIndex(board.height[move]);
					board.makeMove(move);
					tokenList.add(new Token(coordinates[0],board.opponentNumber));
					AnimationThread a = new AnimationThread(coordinates[1],tokenList.get(tokenList.size()-1));
					a.start();
				}
			}
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
		
	}
	
	
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		setBackground(Color.BLACK);

		for(Token token:tokenList){
			token.paintToken(g);
		}
		
		if(engineWin){
			winningLabel.setText("CPU WINS");
			winningLabel.setSize(400,70);
			winningLabel.setFont(new Font("Serif", Font.PLAIN, 48));
			winningLabel.setForeground(Color.MAGENTA);
			winningLabel.setLocation(290, -12);
			winningLabel.setVisible(true);
			winningPlayer = board.engineNumber;
		}
		else if(opponentWin){
			winningLabel.setText("YOU WIN!!");
			winningLabel.setSize(400,70);
			winningLabel.setFont(new Font("Serif", Font.PLAIN, 48));
			winningLabel.setForeground(Color.MAGENTA);
			winningLabel.setLocation(290, -12);
			winningLabel.setVisible(true);
			winningPlayer = board.opponentNumber;
		}
		
		else if(draw){
			winningLabel.setText("DRAW");
			winningLabel.setSize(400,70);
			winningLabel.setFont(new Font("Serif", Font.PLAIN, 48));
			winningLabel.setForeground(Color.MAGENTA);
			winningLabel.setLocation(320, -12);
			winningLabel.setVisible(true);
		}
		if(engineWin || opponentWin){
			Graphics2D g2 = (Graphics2D)label.getGraphics();
			int[] winLine = board.getWinningIndexes(board.board[winningPlayer]);
			if(winLine[2] == 0){
				int x1 = board.getCoordinatesByIndex(winLine[0])[0]+5;
				int y1 = board.getCoordinatesByIndex(winLine[0])[1]+42;
				int x2 = board.getCoordinatesByIndex(winLine[1])[0]+75;
				int y2 = y1;
				g2.setColor(Color.GREEN);
				g2.setStroke(new BasicStroke(12));
				g2.drawLine(x1, y1, x2, y2);
			}
			else if(winLine[2] == 1){
				int x1 = board.getCoordinatesByIndex(winLine[0])[0]+19;
				int y1 = board.getCoordinatesByIndex(winLine[0])[1]+21;
				int x2 = board.getCoordinatesByIndex(winLine[1])[0]+65;
				int y2 = board.getCoordinatesByIndex(winLine[1])[1]+65;
				g2.setColor(Color.GREEN);
				g2.setStroke(new BasicStroke(12));
				g2.drawLine(x1, y1, x2, y2);
			}
			else if(winLine[2] == 2){
				int x1 = board.getCoordinatesByIndex(winLine[0])[0]+15;
				int y1 = board.getCoordinatesByIndex(winLine[0])[1]+70;
				int x2 = board.getCoordinatesByIndex(winLine[1])[0]+63;
				int y2 = board.getCoordinatesByIndex(winLine[1])[1]+23;
				g2.setColor(Color.GREEN);
				g2.setStroke(new BasicStroke(12));
				g2.drawLine(x1, y1, x2, y2);
			}
			else if(winLine[2] == 3){
				int x1 = board.getCoordinatesByIndex(winLine[0])[0]+41;
				int y1 = board.getCoordinatesByIndex(winLine[0])[1]+75;
				int x2 = x1;
				int y2 = board.getCoordinatesByIndex(winLine[1])[1]+5;
				g2.setColor(Color.GREEN);
				g2.setStroke(new BasicStroke(12));
				g2.drawLine(x1, y1, x2, y2);
			}
		}
	}
	
	
	
	
	
	
	private class AnimationThread extends Thread{
		private int destY;
		private Token token;
		
		
		public AnimationThread(int destY,Token token){
			this.destY = destY;
			this.token = token;
		}
		
		
		public void run(){
			MyApplication.undo.setEnabled(false);
			if(MyApplication.sound.isSelected()){
				try{
					AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource("/resources/SOUND 100 MS.wav"));
					Clip clip = AudioSystem.getClip();
					clip.open(audio);
					clip.start();
				}
				catch(UnsupportedAudioFileException uae) {
					System.out.println(uae);
				}
				catch(IOException ioe) {
					System.out.println(ioe);
				}
				catch(LineUnavailableException lua) {
					System.out.println(lua);
				}
			}
			while(token.getYpos() < destY){
				token.setYpos(token.getYpos()+5);
				try{
					Thread.sleep(3);
				}
				catch(Exception e){}
				repaint();
			}
			MyApplication.undo.setEnabled(true);
			if(board.checkWin(board.board[board.engineNumber])){
				engineWin = true;
				MyApplication.resetMenuButtons();
				repaint();
			}
			else if(board.checkWin(board.board[board.opponentNumber])){
				opponentWin = true;
				MyApplication.resetMenuButtons();
				repaint();
			}
			else if(board.checkFullBoard()){
				draw = true;
				MyApplication.resetMenuButtons();
				repaint();
			}
			
			else{
				if((board.ply & 1) == board.engineNumber){ 
					MyApplication.undo.setEnabled(false);
					PlayMove p = new PlayMove();
					p.start();
				}
				else{
					engineTurn = false;
				}
			}
		}
	}
	
	
	
	
	private class PlayMove extends Thread{
		public void run(){
			int move = -1;
			boolean threatFound = false;
			int threat = board.checkImmediateThreat(board.board[board.opponentNumber]);
			if(threat != -1){
				move = board.getColumnByIndex(threat);
				threatFound = true;
			}
			else{
				threat = board.checkImmediateThreat(board.board[board.engineNumber]);
				if(threat != -1){
					move = board.getColumnByIndex(threat);
					threatFound = true;
				}
			}
			if(!threatFound){
				if(MAX_DEPTH == 10){
					engine.tt.initTable();
					board.km.initKillers();
					move = engine.rootNegamax2(MAX_DEPTH, -501, 501, 1);
				}
				else if(MAX_DEPTH == 14){
					engine.tt.initTable();
					board.km.initKillers();
					thinkingLabel.setVisible(true);
					move = engine.rootNegamax(MAX_DEPTH, -501, 501, 1);
				}
				else{
					if(board.engineNumber == 0){
						if(board.ply == 0){
							move = 3;
						}
						else if(board.ply < 8){
							move = board.getColumnByIndex(Book.getMove(toArray(p1Moves), toArray(p2Moves)));
						}
						else if(board.ply == 8 && Book.getMove(toArray(p1Moves), toArray(p2Moves)) != -1){
							move = board.getColumnByIndex(Book.getMove(toArray(p1Moves), toArray(p2Moves)));
						}
						else{
							board.km.initKillers();
							thinkingLabel.setVisible(true);
							move = engine.rootNegamax3(0, -2, 2, 1);
						}
					}
					else{
						if(board.ply <= 6){
							move = board.getColumnByIndex(Book.getMove2(toArray(p1Moves), toArray(p2Moves)));
						}
						else if(board.ply == 7 && Book.getMove2(toArray(p1Moves), toArray(p2Moves)) != -1){
							move = board.getColumnByIndex(Book.getMove2(toArray(p1Moves), toArray(p2Moves)));
						}
						else{
							board.km.initKillers();
							thinkingLabel.setVisible(true);
							move = engine.rootNegamax3(0, -2, 2, 1);
						}
					}
				}
			}
			thinkingLabel.setVisible(false);
			if(!Engine.interrupted){
				if(board.engineNumber == 0){
					p1Moves.add(board.height[move]);
				}
				else{
					p2Moves.add(board.height[move]);
				}
				int[] coordinates = board.getCoordinatesByIndex(board.height[move]);
				board.makeMove(move);
				tokenList.add(new Token(coordinates[0],board.engineNumber));
				AnimationThread a = new AnimationThread(coordinates[1],tokenList.get(tokenList.size()-1));
				a.start();
			}
		}
	}
	
	
	
	
	
	
	
	private int[] toArray(ArrayList<Integer>list){
		int[] x = new int[list.size()];
		for(int i = 0; i<list.size(); i++){
			x[i] = list.get(i);
		}
		return x;
	}
	
	
	
	
	public void undoMove(){
		
			if(engineWin){
				engineWin = false;
				engineTurn = false;
				winningLabel.setVisible(false);
				MyApplication.startGame.setEnabled(false);
				MyApplication.resign.setEnabled(true);
				MyApplication.first.setEnabled(false);
				MyApplication.second.setEnabled(false);
				MyApplication.easy.setEnabled(false);
				MyApplication.intermediate.setEnabled(false);
				MyApplication.hard.setEnabled(false);
			}
			else if(opponentWin){
				opponentWin = false;
				winningLabel.setVisible(false);
				MyApplication.startGame.setEnabled(false);
				MyApplication.resign.setEnabled(true);
				MyApplication.first.setEnabled(false);
				MyApplication.second.setEnabled(false);
				MyApplication.easy.setEnabled(false);
				MyApplication.intermediate.setEnabled(false);
				MyApplication.hard.setEnabled(false);
			}
			else if(draw){
				if(engineTurn){
					engineTurn = false;
				}
				draw = false;
				winningLabel.setVisible(false);
				MyApplication.startGame.setEnabled(false);
				MyApplication.resign.setEnabled(true);
				MyApplication.first.setEnabled(false);
				MyApplication.second.setEnabled(false);
				MyApplication.easy.setEnabled(false);
				MyApplication.intermediate.setEnabled(false);
				MyApplication.hard.setEnabled(false);
			}
			if(board.engineNumber == 0){
				if(p1Moves.size() > 1){
					if(!opponentWin){
						int lastMove = p1Moves.get(p1Moves.size()-1);
						int beforeLastMove = p2Moves.get(p2Moves.size()-1);
						board.unmakeMove(board.getColumnByIndex(lastMove));
						board.unmakeMove(board.getColumnByIndex(beforeLastMove));
						p1Moves.remove(p1Moves.size()-1);
						p2Moves.remove(p2Moves.size()-1);
						tokenList.remove(tokenList.size()-1);
						tokenList.remove(tokenList.size()-1);
					}
					else{
						int lastMove = p2Moves.get(p2Moves.size()-1);
						board.unmakeMove(board.getColumnByIndex(lastMove));
						p2Moves.remove(p2Moves.size()-1);
						tokenList.remove(tokenList.size()-1);
					}
					repaint();
				}
			}
			else{
				if(p1Moves.size() > 0){
					if(!opponentWin){
						int lastMove = p2Moves.get(p2Moves.size()-1);
						int beforeLastMove = p1Moves.get(p1Moves.size()-1);
						board.unmakeMove(board.getColumnByIndex(lastMove));
						board.unmakeMove(board.getColumnByIndex(beforeLastMove));
						p1Moves.remove(p1Moves.size()-1);
						p2Moves.remove(p2Moves.size()-1);
						tokenList.remove(tokenList.size()-1);
						tokenList.remove(tokenList.size()-1);
					}
					else{
						int lastMove = p1Moves.get(p1Moves.size()-1);
						board.unmakeMove(board.getColumnByIndex(lastMove));
						p1Moves.remove(p1Moves.size()-1);
						tokenList.remove(tokenList.size()-1);
					}
					repaint();
				}
			}
			
			
		
	}


}
