
public class NewTT {
	
	
	///The transposition table class with all the necessary operation methods  
	
	private long[] table;
	private final int N = 8306069;
	
	
	public NewTT(){
		table = new long[N];
	}
	
	
	
	public void initTable(){
		for(int i = 0; i<table.length; i++){
			table[i] = 0;
		}
	}
	
	
	
	private int compress(long key){
		return (int)(key % N);
	}
	
	
	
	public void insert(long key,int value,int valueType,int depth){
		int index = compress(key);
		long entry;
		if(value < 0){
			entry = value*-1 + (1 << 9) + (valueType << 10) + (depth << 12) + (key & 9223372036854513664L);
		}
		else{
			entry = value + (valueType << 10) + (depth << 12) + (key & 9223372036854513664L);
		}
		table[index] = entry;
	}
	
	
	
	public long find(long key){
		int index = compress(key);
		if((key & 9223372036854513664L) == (table[index] & 9223372036854513664L)){
			return table[index];
		}
		else{
			return 0;
		}
	}
	
	
	
	
	public int getValue(long entry){
		if(((entry >> 9) & 1L) != 0){
			return (int)(entry & 511L) * -1; 
		}
		else{
			return (int)(entry & 511L);
		}
	}
	
	
	
	
	public int getValueType(long entry){
		return (int)((entry >> 10) & 3L);
	}
	
	
	
	public int getDepth(long entry){
		return (int)((entry >> 12) & 63L);
	}
	
	
	/////////////////////////////////////////////////////////////////////////
	public void insert2(long key,int value,int valueType){
		int index = compress(key);
		long entry;
		if(value == -1){
			entry = 3 + (valueType << 2) + (key & 9223372036854775792L);
		}
		else{
			entry = value + (valueType << 2) + (key & 9223372036854775792L);
		}
		table[index] = entry;
	}
	
	
	
	
	public long find2(long key){
		int index = compress(key);
		if((key & 9223372036854775792L) == (table[index] & 9223372036854775792L)){
			return table[index];
		}
		else{
			return 0;
		}
	}
	
	
	
	
	public int getValue2(long entry){
		if((entry & 3L) == 3L){
			return -1;
		}
		else{
			return (int)(entry & 3L);
		}
	}
	
	
	
	
	public int getValueType2(long entry){
		return (int)((entry >> 2) & 3L);
	}
	
	
	

	

}
