
public class Engine {
	
	private Board board;
	private final int EXACT_VALUE = 1;
	private final int LOWERBOUND = 2;
	private final int UPPERBOUND = 3;
	public NewTT tt;
	public static boolean interrupted;
	
	
	
	public Engine(Board board){
		this.board = board;
		tt = new NewTT();
		
	}
	
	
	//The alpha beta Function for the moves calculation,  3 configurations, one for each level.
	//There is the main negamax() function and the rootNegamax()  which calls negamax() and iterates on the root moves
	
	private int negamax(int depth,int alpha,int beta,int color){ // intermediate level
		while(!interrupted){
			if(board.checkWin(board.board[board.engineNumber])){
				return color*500;
			}
			else if(board.checkWin(board.board[board.opponentNumber])){
				return color*-500;
			}
			else if(depth == 0 || board.checkFullBoard()){
				return color*board.evaluate2();
			}
		
			long entry = tt.find(board.zobristKey);
			if(entry != 0 && tt.getDepth(entry) >= depth){
				if(tt.getValueType(entry) == EXACT_VALUE){
					return tt.getValue(entry);
				}
				else if(tt.getValueType(entry) == LOWERBOUND){
					if(tt.getValue(entry) >= beta){
						return tt.getValue(entry);
					}
				}
				else if(tt.getValueType(entry) == UPPERBOUND){
					if(tt.getValue(entry) <= alpha){
						return tt.getValue(entry);
					}
				}
			}
			boolean exact = false;
			int[] moves = board.getMoves(depth);
			for(int move:moves){
				if(move == -1){
					continue;
				}	
				board.makeMove(move);
				int val = -negamax(depth-1,-beta,-alpha,-color);
				board.unmakeMove(move);

				if(val >= beta){
					board.km.insertKiller(depth, board.height[move]);
					tt.insert(board.zobristKey, val, LOWERBOUND, depth);
					return val;
				}
				if(val > alpha){
					alpha = val;
					exact = true;
				}
			}
			if(exact){
				tt.insert(board.zobristKey, alpha, EXACT_VALUE, depth);
			}
			else{
				tt.insert(board.zobristKey, alpha, UPPERBOUND, depth);
			}
			return alpha;
		}
		return -1000;
	}
	
	
	
	
	public int rootNegamax(int depth,int alpha,int beta,int color){ 
		while(!interrupted){
			int bestMove = -1;
			int[] moves = board.getMoves2(depth);
			for(int move:moves){
				if(move == -1){
					continue;
					}
				board.makeMove(move);
				int val = -negamax(depth-1,-beta,-alpha,-color);
				board.unmakeMove(move);

				if(val > alpha){
					alpha = val;
					bestMove = move;
				}
			}
			return bestMove;
		}
		return -1000;
	}
	
	
	
	
	
	private int negamax2(int depth,int alpha,int beta,int color){// easy level
		while(!interrupted){
			if(board.checkWin(board.board[board.engineNumber])){
				return color*500;
			}
			else if(board.checkWin(board.board[board.opponentNumber])){
				return color*-500;
			}
			else if(depth == 0 || board.checkFullBoard()){
				return color*board.evaluate();
			}
		
			long entry = tt.find(board.zobristKey);
			if(entry != 0 && tt.getDepth(entry) >= depth){
				if(tt.getValueType(entry) == EXACT_VALUE){
					return tt.getValue(entry);
				}
				else if(tt.getValueType(entry) == LOWERBOUND){
					if(tt.getValue(entry) >= beta){
						return tt.getValue(entry);
					}
				}
				else if(tt.getValueType(entry) == UPPERBOUND){
					if(tt.getValue(entry) <= alpha){
						return tt.getValue(entry);
					}
				}
			}
			boolean exact = false;
			int[] moves = board.getMoves(depth);
			for(int move:moves){
				if(move == -1){
					continue;
				}
				board.makeMove(move);
				int val = -negamax2(depth-1,-beta,-alpha,-color);
				board.unmakeMove(move);

				if(val >= beta){
					board.km.insertKiller(depth, board.height[move]);
					tt.insert(board.zobristKey, val, LOWERBOUND, depth);
					return val;
				}
				if(val > alpha){
					alpha = val;
					exact = true;
				}
			}
			if(exact){
				tt.insert(board.zobristKey, alpha, EXACT_VALUE, depth);
			}
			else{
				tt.insert(board.zobristKey, alpha, UPPERBOUND, depth);
			}
			return alpha;
		}
		return -1000;
		
	}
	
	
	
	
	public int rootNegamax2(int depth,int alpha,int beta,int color){
		while(!interrupted){
			int bestMove = -1;
			int[] moves = board.getMoves2(depth);
			for(int move:moves){
				if(move == -1){
					continue;
				}
				board.makeMove(move);
				int val = -negamax2(depth-1,-beta,-alpha,-color);
				board.unmakeMove(move);

				if(val > alpha){
					alpha = val;
					bestMove = move;
				}
			}
			return bestMove;
		}
		return -1000;
	}
	
	
	
	

	
	
	
	
	private int negamax3(int depth,int alpha,int beta,int color){// hard level
		while(!interrupted){
			if(board.checkWin(board.board[board.engineNumber])){
				return color*1;
			}
			else if(board.checkWin(board.board[board.opponentNumber])){
				return color*-1;
			}
			else if(board.checkFullBoard()){
				return 0;
			}
		
			long entry = tt.find2(board.zobristKey);
			if(entry != 0){
				if(tt.getValueType2(entry) == EXACT_VALUE){
					return tt.getValue2(entry);
				}
				else if(tt.getValueType2(entry) == LOWERBOUND){
					if(tt.getValue2(entry) >= beta){
						return tt.getValue2(entry);
					}
				}
				else if(tt.getValueType2(entry) == UPPERBOUND){
					if(tt.getValue2(entry) <= alpha){
						return tt.getValue2(entry);
					}
				}
			}
			boolean exact = false;
			int[] moves = board.getMoves(depth);
			for(int move:moves){
				if(move == -1){
					continue;
				}
				board.makeMove(move);
				int val = -negamax3(depth+1,-beta,-alpha,-color);
				board.unmakeMove(move);

				if(val >= beta){
					board.km.insertKiller(depth, board.height[move]);
					tt.insert2(board.zobristKey, val, LOWERBOUND);
					return val;
				}
				if(val > alpha){
					alpha = val;
					exact = true;
				}
			}
			if(exact){
				tt.insert2(board.zobristKey, alpha, EXACT_VALUE);
			}
			else{
				tt.insert2(board.zobristKey, alpha, UPPERBOUND);
			}
			return alpha;
		}
		return -1000;
	}
	
	
	
	
	public int rootNegamax3(int depth,int alpha,int beta,int color){
		while(!interrupted){
			int bestMove = -1;
			int[] moves = board.getMoves(depth);
			for(int move:moves){
				if(move == -1){
					continue;
				}
				board.makeMove(move);
				int val = -negamax3(depth+1,-beta,-alpha,-color);
				board.unmakeMove(move);

				if(val > alpha){
					alpha = val;
					bestMove = move;
				}
			}
			return bestMove;
		}
		return -1000;
	}

	
	
	



	

}
